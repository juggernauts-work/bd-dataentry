<?php
session_start();
if(!isset($_SESSION["assid"])){
	header("Location:login.php");
}
if(isset($_SESSION["type"])){
	header("Location:admin.php");
}
$conn=mysqli_connect("localhost","root","","bfsi");
if(!$conn){
	echo "Problem with connection";
}
$CRD_No=$_GET["crdno"];
$id=$_SESSION["assid"];
$name=$_SESSION["name"];
$getcom="select company_name from bd_master where CRD_No='$CRD_No'";
$res=mysqli_query($conn,$getcom);
$row=mysqli_fetch_assoc($res);
?>
<html>
<head>

	<title>FORM</title>
	 <meta name="viewport" content="width=device-width, initial-scale=1">

      <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/dashboard.css"/>
  <style>
  
/*  bhoechie tab */
div.bhoechie-tab-container{
  z-index: 10;
  background-color: #ffffff;
  padding: 0 !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  border:1px solid #ddd;
  margin-top: 20px;
  margin-left: 50px;
  -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  box-shadow: 0 6px 12px rgba(0,0,0,.175);
  -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  background-clip: padding-box;
  opacity: 0.97;
  filter: alpha(opacity=97);
  position:relative;
  top:100px;
  left:-80px;
  width:1250px;
}
div.bhoechie-tab-menu{
  padding-right: 0;
  padding-left: 0;
  padding-bottom: 0;
  width:300px;
}
div.bhoechie-tab-menu div.list-group{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a .glyphicon,
div.bhoechie-tab-menu div.list-group>a .fa {
  color: #5D92BA;
}
div.bhoechie-tab-menu div.list-group>a:first-child{
  border-top-right-radius: 0;
  -moz-border-top-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a:last-child{
  border-bottom-right-radius: 0;
  -moz-border-bottom-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a.active,
div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
div.bhoechie-tab-menu div.list-group>a.active .fa{
  background-color: #5D92BA;
  background-image: #5D92BA;
  color: #ffffff;
}
div.bhoechie-tab-menu div.list-group>a.active:after{
  content: '';
  position: absolute;
  left: 100%;
  top: 50%;
  margin-top: -13px;
  border-left: 0;
  border-bottom: 13px solid transparent;
  border-top: 13px solid transparent;
  border-left: 10px solid #5D92BA;
}

div.bhoechie-tab-content{
  background-color: #ffffff;
  /* border: 1px solid #eeeeee; */
  padding-left: 20px;
  padding-top: 10px;
  margin-top:30px;
}

div.bhoechie-tab div.bhoechie-tab-content:not(.active){
  display: none;
}
div.footer{
	height:40px;
	width:100%;
	background-color:#5d92ba;
	position:relative;
	top:200px;
	
}
  </style>
</head>
<body>
<script>
var editing_current;
var firm_addresses=0;
var firm_affliationdtls=0;
var firm_arrangement=0;
var firm_business_types=0;
var firm_other_business_types=0;
var firm_disclosure_events=0;
var firm_do_eo=0;
var firm_indirectowners=0;
var firm_operations_summary=0;
var firm_other_names=0;
var firm_profile=0;
var firm_reg_dates=0;
var firm_sec_reg=0;
var crdnodirect=<?php echo $CRD_No;?>;//  put it 25 for a same crd no later take it directly from the database
var firm_reg_dates_cntr=0;
var firm_other_names_cntr=0;
var firm_do_eo_cntr=0;
var firm_other_business_types_cntr=0;
var disclosure_cntr=0;
var firm_indirectowners_cntr=0;
var firm_on_del_arr=[];
var firm_do_eo_ondel=[];
var firm_io_ondel=[];
$(document).ready(function(){
	$("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
		$("form").each(function(){// sets all input elements not editable initially
    $(this).find(':input').attr("readonly","true");
});
$('.btn.btn-primary').css('display','block');
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
	
	/*$("#firm_reg_dates_btn").click(function(e){//firm reg dates add entry button call
		e.preventDefault();
		firm_reg_dates_cntr=firm_reg_dates_cntr + 1;
		var ele="<center><div><input type='text' id='frdrtn"+firm_reg_dates_cntr+"' placeholder='reg_type_name' style='margin:5px; left:115px;position:relative; float:left;width:500px; height:40px;'/><br><textarea id='frddtls"+firm_reg_dates_cntr+"' cols='100' rows='10' style='margin:5px;'></textarea><br><button class='btn btn-warning' >Save</button></div>";
			$('#reg_dates_add_here').append(ele);
	});*/
	
	$("form").each(function(){// sets all input elements not editable initially
    $(this).find(':input').attr("readonly","true");
});

     
	
	$('.btn.btn-primary').click(function(){// clicked edit 
	   $("form").each(function(){
    $(this).find(':input').attr("readonly",null);
                            });
	
	    $('.btn.btn-success').css("display","block");
		$(this).css("display","none");
		
	});
	
	$('.btn.btn-success').click(function(){
		 $("form").each(function(){
    $(this).find(':input').attr("readonly","true");
                            });
	
	    $('.btn.btn-primary').css("display","block");
		$(this).css("display","none");
	});
	
	 
	
	
	
	$('#finalsubmit').click(function(){
		//alert("submit clicked");
		
		//alert(firm_addresses+""+firm_affliationdtls+""+firm_arrangement+""+firm_business_types+""+firm_disclosure_events+""+firm_do_eo+""+firm_indirectowners+
		//""+firm_operations_summary+""+firm_other_names+""+firm_profile+""+ firm_reg_dates +""+firm_sec_reg);
		
	// submitted forms which he wanted to edit
	
	if(firm_addresses!=0){ //this indicates the save for firm_addresses is clicked
		//var crd=$("#facrd").val().replace( /\s/g, "" );
		//var addid=$("#famnai").val().replace( /\s/g, "" );
	//	var addtype=$("#famnat").val();
		var addline1=$("#famnal1").val().replace( /\s/g, "" );
		var addline2=$("#famnal2").val().replace( /\s/g, "" );
		var addline3=$("#famnal3").val().replace( /\s/g, "" );
		var city=$("#famnct").val().replace( /\s/g, "" );
		var state=$("#famnst").val().replace( /\s/g, "" );
		var country=$("#famncn").val().replace( /\s/g, "" );
		var zipcode=$("#famnz").val().replace( /\s/g, "" );
		//alert(crd+""+addid+""+addtype+""+addline1+""+addline2+""+addline3+""+city+""+state+""+country+""+zipcode);
		var url="insert into firm_addresses values(null,'"+crdnodirect+"','12','2017',null,'main office','"+addline1+"','"+addline2+"','"+addline3+"','"+city+"','"+state+"','"+country+"','"+zipcode+"');";
		$.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 
		 });
		 
		// var addid=$("#famlai").val().replace( /\s/g, "" );
		//var addtype=$("#famlat").val().replace( /\s/g, "" );
		var addline1=$("#famlal1").val().replace( /\s/g, "" );
		var addline2=$("#famlal2").val().replace( /\s/g, "" );
		var addline3=$("#famlal3").val().replace( /\s/g, "" );
		var city=$("#famlct").val().replace( /\s/g, "" );
		var state=$("#famlst").val().replace( /\s/g, "" );
		var country=$("#famlcn").val().replace( /\s/g, "" );
		var zipcode=$("#famlz").val().replace( /\s/g, "" );
		//alert(crd+""+addid+""+addtype+""+addline1+""+addline2+""+addline3+""+city+""+state+""+country+""+zipcode);
		var url="insert into firm_addresses values("+null+",'"+crdnodirect+"','12','2017',null,'mailing office','"+addline1+"','"+addline2+"','"+addline3+"','"+city+"','"+state+"','"+country+"','"+zipcode+"');";
		$.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
		 
		 
		 firm_addresses=0;
	}
	if(firm_affliationdtls!=0){// working
	//	var bdname=$("#fadbdn").val().replace( /\s/g, "" );
	//	var pdflink=$("#fadpdl").val().replace( /\s/g, "" );
	    var bdname="company name sample";
		var pdflink="pdf link sample";
		var pdfcrd=$("#fadpc").val().replace( /\s/g, "" );
		var name=$("#fadn").val().replace( /\s/g, "" );
		//var crdno=$("#fadcrd").val().replace( /\s/g, "" );
		var type=$("#fadty").val().replace( /\s/g, "" );
		var businessadd=$("#fadba").val().replace( /\s/g, "" );
		var effdate=$("#faded").val().replace( /\s/g, "" );
		var desc=$("#fadde").val().replace( /\s/g, "" );
		var forent=$("#fadfe").val().replace( /\s/g, "" );
		var cntry=$("#fadco").val().replace( /\s/g, "" );
		var secact=$("#fadsa").val().replace( /\s/g, "" );
		var inveadv=$("#fadid").val().replace( /\s/g, "" );
		var url="insert into firm_affliationdtls values('"+bdname+"','"+pdflink+"','"+pdfcrd+"','"+name+"','"+crdnodirect+"','"+type+"','12','2017','"+businessadd+"','"+effdate+"','"+desc+"','"+forent+"','"+cntry+"','"+secact+"','"+inveadv+"');";
		$.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
		// alert("edited");
		firm_affliationdtls=0;
	}
	if(firm_arrangement!=0){//problem
	//	var crdnonew=$("#farrcrd").val().replace( /\s/g, "" );
	//alert('success');
		var icaob = $("input[name='faicab']:checked").val();
		var iiaob = $("input[name='faiiao']:checked").val();
		var abrbtp=$("input[name='faabbtp']:checked").val();
		var aafsftp = $("input[name='faaabt']:checked").val();
		var acafsftp = $("input[name='faacbtp']:checked").val();
		var iicmp=$("input[name='faiicmp']:checked").val();
		var iiffb = $("input[name='faiiff']:checked").val();
		var url="insert into firm_arrangements values('"+crdnodirect+"','"+icaob+"','"+iiaob+"','"+abrbtp+"','"+aafsftp+"','"+acafsftp+"','"+iicmp+"','"+iiffb+"','12','2017');";
		$.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
		 
		 firm_arrangement=0;
		 
	}
	
	if(firm_business_types!=0){
		//var fbtcrdno=$("#fbtcrd").val().replace( /\s/g, "" );
		var iecb=$("input[name='fbtiecm']:checked").val().replace( /\s/g, "" );
		var iefa=$("input[name='fbtiefa']:checked").val().replace( /\s/g, "" );
		var ibico=$("input[name='fbtibico']:checked").val().replace( /\s/g, "" );
		var ibrco=$("input[name='fbtibrco']:checked").val().replace( /\s/g, "" );
		var ibscs=$("input[name='fbtibscs']:checked").val().replace( /\s/g, "" );
		var iuse=$("input[name='fbtisuse']:checked").val().replace( /\s/g, "" );
		var iusm=$("input[name='fbtiusm']:checked").val().replace( /\s/g, "" );
		var imr=$("input[name='fbtimr']:checked").val().replace( /\s/g, "" );
		var idu=$("input[name='fbtidu']:checked").val().replace( /\s/g, "" );
		var ibu=$("input[name='fbtibu']:checked").val().replace( /\s/g, "" );
		var idm=$("input[name='fbtidm']:checked").val().replace( /\s/g, "" );
		var ibm=$("input[name='fbtibm']:checked").val().replace( /\s/g, "" );
		var ibsva=$("input[name='fbtisbsva']:checked").val().replace( /\s/g, "" );
		var ist=$("input[name='fbtist']:checked").val().replace( /\s/g, "" );
		var irs=$("input[name='fbtirs']:checked").val().replace( /\s/g, "" );
		var ibso=$("input[name='fbtibso']:checked").val().replace( /\s/g, "" );
		var ibo=$("input[name='fbtibo']:checked").val().replace( /\s/g, "" );
		var ibss=$("input[name='fbtibss']:checked").val().replace( /\s/g, "" );
		var ibsn=$("input[name='fbtibsn']:checked").val().replace( /\s/g, "" );
		var iias=$("input[name='fbtiias']:checked").val().replace( /\s/g, "" );
		var ibstlp=$("input[name='fbtibstlp']:checked").val().replace( /\s/g, "" );
		var ibstls=$("input[name='fbtibstls']:checked").val().replace( /\s/g, "" );
		var inal=$("input[name='fbtinal']:checked").val().replace( /\s/g, "" );
		var ipto=$("input[name='fbtipt']:checked").val().replace( /\s/g, "" );
		var ips=$("input[name='fbtipps']:checked").val().replace( /\s/g, "" );
		var ibsm=$("input[name='fbtibsm']:checked").val().replace( /\s/g, "" );
		var ibnb=$("input[name='fbtibnb']:checked").val().replace( /\s/g, "" );
		var ibni=$("input[name='fbtibni']:checked").val().replace( /\s/g, "" );
		var iob=$("input[name='fbtiob']:checked").val().replace( /\s/g, "" );
		
		var url="insert into firm_business_types values('"+crdnodirect+"','12','2017','"+iecb+"','"+iefa+"','"+ibico+"','"+ibrco+"','"+ibscs+"','"+iuse+"','"+iusm+"','"+imr+"','"+idu+"','"+ibu+"','"+idm+"','"+ibm+"','"+ibsva+"','"+ist+"','"+irs+"','"+ibso+"','"+ibo+"','"+ibss+"','"+ibsn+"','"+iias+"','"+ibstlp+"','"+ibstls+"','"+inal+"','"+ipto+"','"+ips+"','"+ibsm+"','"+ibnb+"','"+ibni+"','"+iob+"');";
	    $.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
		 
		 
		 firm_business_types=0;
	}
	if(firm_disclosure_events!=0){
	/*	var fdecrd=$("#fdecrd").val().replace( /\s/g, "" );
		var fdeaed=$("input[name='fdeaed']:checked").val().replace( /\s/g, "" ); 
        var fdertne=$("#fdertne").val().replace( /\s/g, "" );	
        var url="insert into firm_disclosure_events values('"+fdecrd+"','"+fdeaed+"','"+fdertne+"','12','2017');";	
$.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });		*/
		 if($("input[name='fdeaed']:checked").val()=='no'){
			 var url="insert into firm_disclosure_events values(null,"+crdnodirect+",'no',null,'1','12',null);";
			  $.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });	
		 }else{
			 var a=1;
			 while(a<=disclosure_cntr){
				 if(del_fdie_arr.includes(a)){
					 a=a+1;
					 continue;
				 }
				 var e = document.getElementById("disc_events_select"+a+"");
                 var strUser = e.options[e.selectedIndex].text;
				 var count=$("#disc_events_count"+a+"").val().replace( /\s/g, "" );
				 var url="insert into firm_disclosure_events values(null,"+crdnodirect+",'yes','"+strUser+"','1','12','"+count+"');";
				 $.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });	
		 a=a+1;
			 }
		 }
		 
		firm_disclosure_events=0; 
	}
	if(firm_do_eo!=0){
	/*	var crdno=$("#fdecrdno").val().replace( /\s/g, "" );
		var den=$("#fdeden").val().replace( /\s/g, "" );
		var deln=$("#fdedeln").val().replace( /\s/g, "" );
		var decn=$("#fdedecn").val().replace( /\s/g, "" );
		var det=$("#fdedet").val().replace( /\s/g, "" );
		var p=$("#fdedep").val().replace( /\s/g, "" );
		var psd=$("#fdepsd").val().replace( /\s/g, "" );
		var po=$("#fdepo").val().replace( /\s/g, "" );
		var ddmp=$("input[name='fdeddmp']:checked").val().replace( /\s/g, "" );
		var iprc=$("input[name='fdeiprc']:checked").val().replace( /\s/g, "" );
		var url="insert into firm_do_eo values('"+crdno+"','"+den+"','12','2017','"+deln+"','"+decn+"','"+det+"','"+p+"','"+psd+"','"+po+"','"+ddmp+"','"+iprc+"');";
	    $.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });	*/
		 var a=1;
		 while(a<=firm_do_eo_cntr){
			 if(firm_do_eo_ondel.includes(a)){
				 a=a+1;
				 continue;
			 }
			 var val1=$("#deln"+a+"").val().replace( /\s/g, "" );
			 var val2=$("#decn"+a+"").val().replace( /\s/g, "" );
			 var val3=$("#det"+a+"").val().replace( /\s/g, "" );
			 var val4=$("#dep"+a+"").val().replace( /\s/g, "" );
			 var val5=$("#depst"+a+"").val().replace( /\s/g, "" );
			 var val6=$("#depo"+a+"").val().replace( /\s/g, "" );
			 var ddmp=$("input[name='deddmp"+a+"']:checked").val().replace( /\s/g, "" );
			 var ispr=$("input[name='deiprc"+a+"']:checked").val().replace( /\s/g, "" );
			 var url="insert into firm_do_eo values(null,"+crdnodirect+",'1','12','"+val1+"','"+val2+"','"+val3+"','"+val4+"','"+val5+"','"+val6+"','"+ddmp+"','"+ispr+"');";
			  $.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
		 a=a+1;
		 }
		 
		 firm_do_eo=0;
	}
	if(firm_indirectowners!=0){
		/*var crdno=$("#fiocrd").val().replace( /\s/g, "" );
		var idono=$("#fioido").val().replace( /\s/g, "" );
		var iln=$("#fioiln").val().replace( /\s/g, "" );
		var icn=$("#fioicn").val().replace( /\s/g, "" );
		var it=$("#fioit").val().replace( /\s/g, "" );
		var iv=$("#fioiv").val().replace( /\s/g, "" );
		var rtd=$("#fiortd").val().replace( /\s/g, "" );
		var red=$("#fiored").val().replace( /\s/g, "" );
		var po=$("#fiopo").val().replace( /\s/g, "" );
		var ddmp=$("input[name='fioddmp']:checked").val().replace( /\s/g, "" );
		var prc=$("input[name='fioiprc']:checked").val().replace( /\s/g, "" );
		var url="insert into firm_indirectowners values('"+crdno+"','"+idono+"','"+iln+"','"+icn+"','12','2017','"+it+"','"+iv+"','"+rtd+"','"+red+"','"+po+"','"+ddmp+"','"+prc+"');";
		 $.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });*/
		 var a=1;
		 while(a<=firm_indirectowners_cntr){
			 if(firm_io_ondel.includes(a)){
				 a=a+1;
				 continue;
			 }
		 var fioin=$("#fioin"+a+"").val().replace( /\s/g, "" );
		 var fioiln=$("#fioiln"+a+"").val().replace( /\s/g, "" );
		 var fioicn=$("#fioicn"+a+"").val().replace( /\s/g, "" );
		 var fioit=$("#fioit"+a+"").val().replace( /\s/g, "" );
		 var fioiv=$("#fioiv"+a+"").val().replace( /\s/g, "" );
         var fiortd=$("#fiortd"+a+"").val().replace( /\s/g, "" );
		 var fiorsd=$("#fiorsd"+a+"").val().replace( /\s/g, "" );
		 var fiopo=$("#fiopo"+a+"").val().replace( /\s/g, "" );
		 var fioddmp=$("input[name='fioddmp"+a+"']:checked").val();
		  var fioiprc=$("input[name='fioiprc"+a+"']:checked").val();
		  var url="insert into firm_indirectowners values(null,'"+crdnodirect+"','"+fioin+"','"+fioiln+"','"+fioicn+"','1','12','"+fioit+"','"+fioiv+"','"+fiortd+"','"+fiorsd+"','"+fiopo+"','"+fioddmp+"','"+fioiprc+"');";
		  $.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
		 a=a+1;
		 }
		 
		 firm_indirectowners=0;
	}
	if(firm_operations_summary!=0){//working
	 // var foscrd=$("#foscrd").val().replace( /\s/g, "" );
	  var irws=$("input[name='fosirws']:checked").val().replace( /\s/g, "" );
	  var irwsr=$("input[name='fosirwso']:checked").val().replace( /\s/g, "" );
	  var iswst=$("input[name='fosirwst']:checked").val().replace( /\s/g, "" );
	  var nsr=$("#fosnsr").val().replace( /\s/g, "" );
	  var nstr=$("#fosnstr").val().replace( /\s/g, "" );
	  var ics=$("input[name='fosics']:checked").val().replace( /\s/g, "" );
	  var fosntb=$("#fosntb").val().replace( /\s/g, "" );
	  var iadii=$("input[name='fosiafii']:checked").val().replace( /\s/g, "" );
	  var hraob=$("input[name='foshraob']:checked").val().replace( /\s/g, "" );
	  var url="insert into firm_operations_summary values('"+crdnodirect+"','"+irws+"','"+irwsr+"','"+iswst+"','"+nsr+"','"+nstr+"','12','2017','"+ics+"','"+fosntb+"','"+iadii+"','"+hraob+"');";
	  $.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
		 
		 firm_operations_summary=0;
	}
	if(firm_other_business_types!=0){
		
		var a=1;
		
		while(a<=firm_other_business_types_cntr){
			if(firm_obt_ondel.includes(a)){
				a=a+1;
				continue;
			}
			var ob=$("#firmobtob"+a+"").val().replace( /\s/g, "" );
			var obd=$("#firmobbd"+a+"").val().replace( /\s/g, "" );
			var url="insert into firm_otherbusiness_types values('"+crdnodirect+"',"+null+",'"+ob+"','"+obd+"',1,12);";
			 $.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
		 a=a+1;
		}
		
		firm_other_business_types=0;
	}
	if(firm_other_names!=0){
	
		 
		 for(var i=1; i<=firm_other_names_cntr;i++){
			 if(firm_on_del_arr.includes(i)){
				 continue;
			 }
			 var oname= $("#firmoname"+i+"").val().replace( /\s/g, "" );
			 var stlist=$("#firmonsl"+i+"").val().replace( /\s/g, "" );
			var arr=stlist.split(",");
			var url="insert into firm_other_names values";
			var a=0;
			while(a<=arr.length-1){
				if(a<arr.length-1)
				url=url.concat("("+crdnodirect+",'"+oname+"','"+arr[a]+"','1','12',null),");
			     else
					url= url.concat("("+crdnodirect+",'"+oname+"','"+arr[a]+"','1','12',null);");
				a=a+1;
			}
			$.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
			 
		 }
		 
		 firm_other_names=0;
		 
	}
	if(firm_profile!=0){//working
	alert("entered");
		var fn=$("#fpfn").val().replace( /\s/g, "" );
		var ffln=$("#fpffln").val().replace( /\s/g, "" );
		var fdba=$("#fpfdba").val().replace( /\s/g, "" );
		//var crdno=$("#fpcn").val().replace( /\s/g, "" );
		var secno=$("#fpsn").val().replace( /\s/g, "" );
		var fc=$("#fpfc").val().replace( /\s/g, "" );
		var ffd=$("#fpffd").val().replace( /\s/g, "" );
		var ffl=$("#fpffl").val().replace( /\s/g, "" );
		var ffye=$("#fpffe").val().replace( /\s/g, "" );
		var ffbtl=$("#fpbtn").val().replace( /\s/g, "" );
		var rb=$("#fprb").val().replace( /\s/g, "" );
		var url="insert into firm_profile values('"+fn+"','"+ffln+"','"+fdba+"','"+secno+"','"+crdnodirect+"','12','2017','"+fc+"','"+ffd+"','"+ffl+"','"+ffye+"','"+ffbtl+"','"+rb+"');";
		$.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
		 
		 firm_profile=0;
	}
	if(firm_reg_dates!=0){
		
		
	/*	var frcrd=$("#frdcrd").val().replace( /\s/g, "" );
		var frdrsn=$("#frdrsn").val().replace( /\s/g, "" );
		var frdrtn=$("#frdrtn").val().replace( /\s/g, "" );
		var frdrbn=$("#frdrbn").val().replace( /\s/g, "" );
		var frdrs=$("#frdrs").val().replace( /\s/g, "" );
		var frdred=$("#frdred").val().replace( /\s/g, "" );
		var url="insert into firm_reg_dates values('"+frcrd+"','"+frdrsn+"','"+frdrtn+"','"+frdrbn+"','"+frdrs+"','"+frdred+"','12','2017');"
		$.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });*/
		 var sa=$("input[name='frdsa']:checked").val();
		 var sdate=$("#secdate").val();
		 var fa=$("input[name='fadfa']:checked").val();
		 var fdate=$("#finradate").val();
		 var url="insert into firm_reg_dates values(null,'"+crdnodirect+"','sec','sec','"+sa+"','"+sdate+"','1','12');"
		$.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
		 
		 
		 //sro
         var us=$("#srotf").val();
		 //var indval=us.split("");
		 var indval=us.split(/\r?\n/); 
		 var a=0;
		 
		 var url="insert into firm_reg_dates values";
		 while(a<=indval.length-1){
			 if(a!=indval.length-3)
			 url=url.concat("(null,'"+crdnodirect+"','SRO','"+indval[a]+"','"+indval[a+1]+"','"+indval[a+2]+"','1','12'),");
		     else{
				 url=url.concat("(null,'"+crdnodirect+"','SRO','"+indval[a]+"','"+indval[a+1]+"','"+indval[a+2]+"','1','12');");
			 }
			 a=a+3;
		 }
		 
		 
		 $.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
		 
		 
		 //us states and territories
		 var us1=$("#uster").val();
		 //var indval=us.split("");
		 var indval1=us1.split(/\r?\n/);
		 
		 var a=0;
		 
		 var url1="insert into firm_reg_dates values";
		 while(a<=indval1.length-1){
			 if(a!=indval1.length-3)
			 url1=url1.concat("(null,'"+crdnodirect+"','us state territories','"+indval1[a]+"','"+indval1[a+1]+"','"+indval1[a+2]+"','1','12'),");
		     else{
				 url1=url1.concat("(null,'"+crdnodirect+"','us state territories','"+indval1[a]+"','"+indval1[a+1]+"','"+indval1[a+2]+"','1','12');");
			 }
			 a=a+3;
		 }
		 
		 
		 $.ajax({
         type: "post",
		 data:{url:url1},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
		 
		/* var a=0;
		 var url="insert into firm_reg_dates values";
		 while(a<=indval.length-1){
			//myArray = indval[a+1].split("(?<=\\D)(?=\\d)");
			 //alert(indval[a]);
			// url=url.concat("(null,'"+crdnodirect+"','us states and territories','"+val1+"','"+val2+"','1','12'),");
			alert(indval[a]+""+indval[a+1]);
			 a=a+2;
		 }*/

		 firm_reg_dates=0;
	}
	if(firm_sec_reg!=0){//working
		//var crdno=$("#fsrcn").val().replace( /\s/g, "" );
		var fsrib=$("input[name='fsrib']:checked").val().replace( /\s/g, "" );
		var fsricag=$("input[name='fsricag']:checked").val().replace( /\s/g, "" );
		var fsigo=$("input[name='fsigo']:checked").val().replace( /\s/g, "" );
		var fsigc=$("input[name='fsigc']:checked").val().replace( /\s/g, "" );
		var url="insert into firm_sec_reg values('"+crdnodirect+"','"+fsrib+"','"+fsricag+"','"+fsigo+"','"+fsigc+"','12','2017');";
		$.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
		firm_sec_reg=0;
	}
	
	  
	  
	});
	
	$("#add_firmon").click(function(e){
		e.preventDefault();
		firm_other_names_cntr=firm_other_names_cntr+1;
		var ele="<div class='panel panel-default' id='pan"+firm_other_names_cntr+"'><div class='panel-body'><label style='position:relative;left:-155px;'>OTHER NAME:</label><input type='text' id='firmoname"+firm_other_names_cntr+"' style='width:300px; height:30px; margin-top:20px; position:relative;left:-146px;' /><br><textarea id='firmonsl"+firm_other_names_cntr+"' rows='10' cols='100' style=' margin-top:10px;resize:none ' placeholder='paste states list here' onkeydown='firm_other_names=1;'/></div><div class='panel-footer'><button class='btn btn-danger' type='button' onclick='delfon("+firm_other_names_cntr+");'>delete</button></div> </div>";
		$('#add_firmon_addhere').append(ele);
		$('#saved_addfirmon').css('display','inline-block');
		
	
	});
	
	$('#firm_do_eo_btn').click(function(){
		firm_do_eo_cntr=firm_do_eo_cntr+1;
		$('.btn.btn-success').css('display','block');
		var ele="<div class='panel panel-default' id='pnlfde"+firm_do_eo_cntr+"'><div class='panel-body'><div style='margin-top:20px; position:relative;float:left;'>DO EO LEGAL NAME :  <input type='text' id='deln"+firm_do_eo_cntr+"' onkeydown='firm_do_eo=1;'/></div><div style='position:relative;float:left; margin-top:20px; margin-left:200px;'>DO EO CRD NO: <input type='text' id='decn"+firm_do_eo_cntr+"' onclick='firm_do_eo=1;'/></div><div style='position:relative;float:left; margin-top:28px;'>DO EO TYPE: <input type='text' id='det"+firm_do_eo_cntr+"' style='position:relative;left:56px;' onclick='firm_do_eo=1;'/></div><div style='position:relative;float:left; margin-top:28px; margin-left:256px;'>POSITION: <input type='text' id='dep"+firm_do_eo_cntr+"' style='position:relative;left:38px;' onclick='firm_do_eo=1;'/></div><div style='position:relative;float:left; margin-top:30px; '>POSITION START DATE:<input type='text' id='depst"+firm_do_eo_cntr+"' onclick='firm_do_eo=1;' placeholder='mm/yyyy'/></div><div style='position:relative;float:left;margin-top:35px;margin-left:192px;'>PERC OWNERSHIP:  <select id='depo"+firm_do_eo_cntr+"'><option value='volvo'>Volvo</option><option value='saab'>Saab</option><option value='mercedes'>Mercedes</option><option value='audi'>Audi</option></select> </div><br><div style='position:relative;float:left;margin-top:84px; margin-left:-692px;'> DOES DIRECT MANAGEMENT POLICIES : <input type='radio' name='deddmp"+firm_do_eo_cntr+"'  value='yes'>YES</input> <input type='radio' name='deddmp"+firm_do_eo_cntr+"'  value='no'>NO </input></div><div style='position:relative;float:left;margin-top:83px; margin-left:-219px;'> IS PUBLIC REPORTING COMPANY : <input type='radio' name='deiprc"+firm_do_eo_cntr+"'  value='yes'>YES</input> <input type='radio' name='deiprc"+firm_do_eo_cntr+"'  value='no'>NO </input></div></div><div class='panel-footer'><button type='button' class='btn btn-danger' onclick='delfde("+firm_do_eo_cntr+")'>delete</button></div></div>";
		

$('#fde_add_here').append(ele);
			
			
			
			
	});
	
	
	$('#fobt_add_btn').click(function(){
		firm_other_business_types_cntr=firm_other_business_types_cntr+1;
		$('.btn.btn-success').css('display','block');
		var ele="<div class='panel panel-default' id='pnlobt"+firm_other_business_types_cntr+"'><div class='panel-body'><br><textarea rows='10' cols='50' style='margin-right:50px;margin-left:20px;' placeholder='Other business' id='firmobtob"+firm_other_business_types_cntr+"' onkeydown='firm_other_business_types=1;'/> <textarea rows='10' cols='50' placeholder='business description(if any)' id='firmobbd"+firm_other_business_types_cntr+"' onclick='firm_other_business_types=1;'/></div><div class='panel-footer'><button class='btn btn-danger' type='button' onclick='delobt("+firm_other_business_types_cntr+");'>delete</button></div></div>";
		$('#fobt_add_here').append(ele);
		
	});
	
	$('#disclosure_btn').click(function(){
		disclosure_cntr=disclosure_cntr+1;
		//var e = document.getElementById("ddlViewBy");
        //var strUser = e.options[e.selectedIndex].text;
		var ele="<div class='panel panel-default' id='pnlfdire"+disclosure_cntr+"'><div class='panel-body'>Event name :  <select id='disc_events_select"+disclosure_cntr+"' onchange='changedoption("+disclosure_cntr+")'><option disabled selected value>--select an option--</option><option value='volvo'>Volvo</option><option value='saab'>Saab</option><option value='mercedes'>Mercedes</option><option value='audi'>Audi</option></select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COUNT:<input type='number' id='disc_events_count"+disclosure_cntr+"' z-index:1;' onkeydown='firm_disclosure_events=1;'/><button type='button' class='btn btn-danger' onclick='btndelfde("+disclosure_cntr+")' style='float:right;'>delete</button></div></div><br>";
		$('#reg_discevents_add_here').append(ele);
		
	});
	
	$('#firm_indirectowners_btn').click(function(){
		firm_indirectowners_cntr=firm_indirectowners_cntr+1;
		$('.btn.btn-success').css('display','block');
		var ele="<div class='panel panel-default' id='pnlfio"+firm_indirectowners_cntr+"'><div class='panel-body' style='max-height:340px;'><div style='margin:20px;position: relative;left: -1px;top: -43px; margin-top:61px;'>IDO LEGAL NAME:<input type='text' id='fioiln"+firm_indirectowners_cntr+"' style='position:relative;left:38px;' onkeydown='firm_indirectowners=1;'/></div><div style='margin:20px;top:-39px;position: relative;'>	IDO CRD NO:<input type='text' id='fioicn"+firm_indirectowners_cntr+"' style='position:relative;left:70px;' onkeydown='firm_indirectowners=1;'/></div><div style='margin:20px; position: relative;left: 414px;top: -85px;'>IDO TYPE:<input type='text' id='fioit"+firm_indirectowners_cntr+"' style='position:relative;left:88px;' onkeydown='firm_indirectowners=1;'/></div><div style='margin:20px; position: relative;top: -79px;'>IDO VIA:<input type='text' id='fioiv"+firm_indirectowners_cntr+"' style='position:relative;left:103px;' onkeydown='firm_indirectowners=1;'/></div><div style='margin:20px; position: relative;left: 414px;top: -126px;'>REL TO DO:<input type='text' id='fiortd"+firm_indirectowners_cntr+"' style='position:relative;left:75px;' onkeydown='firm_indirectowners=1;'/></div><div style='margin:20px; position: relative;top: -114px;'>REL EST DATE:<input type='text' id='fiorsd"+firm_indirectowners_cntr+"' onkeydown='firm_indirectowners=1;' style='position:relative;left:55px;' placeholder='mm/yyyy'/>	</div><div style='margin:20px; position: relative;left: 414px;top: -158px;'>PERC OWNERSHIP: <select id='fiopo"+firm_indirectowners_cntr+"'><option value='volvo'>Volvo</option><option value='saab'>Saab</option><option value='mercedes'>Mercedes</option><option value='audi'>Audi</option></select> </div><div style='margin:20px;position: relative;top: -147px;'>    DOES DIRECT MANAGEMENT POLICIES : <input type='radio' name='fioddmp"+firm_indirectowners_cntr+"' value='yes'/>YES <input type='radio' name='fioddmp"+firm_indirectowners_cntr+"' value='no'/>NO</div><div style='margin:20px;position: relative;top: -150px;'>IS PUBLIC REPORTING COMPANY     : <input type='radio' name='fioiprc"+firm_indirectowners_cntr+"' value='yes'/>YES <input type='radio' name='fioiprc"+firm_indirectowners_cntr+"' value='no'/>NO</div></div><div class='panel-footer'><button class='btn btn-danger' type='button' onclick='fiood("+firm_indirectowners_cntr+")'>delete</button></div></div>";
	     $('#fio_add_here').append(ele);
		 });
	
});

		var del_fdie_arr=[];
	function btndelfde(a){
		del_fdie_arr.push(a);
		//alert(del_fdie_arr);
		document.getElementById("pnlfdire"+a).style.display="none";
	}
	function changedoption(a){
      var valuea=document.getElementById("disc_events_select"+a).value;
	  var b=1;
	  while(b<=disclosure_cntr && b!=a){
		  if(document.getElementById("disc_events_select"+b).value==valuea){
			  alert("event already selected");
			  document.getElementById("disc_events_select"+a).value=null;
		  }
		  b=b+1;
	  }
	}
	function delfon(a){
		firm_on_del_arr.push(a);
		//alert(firm_on_del_arr);
		document.getElementById("pan"+a).style.display="none";
	}
	function delfde(a){
		firm_do_eo_ondel.push(a);
		//alert(firm_do_eo_ondel);
		document.getElementById("pnlfde"+a).style.display="none";
	}
	function fiood(a){
		firm_io_ondel.push(a);
		//alert(firm_io_ondel);
		document.getElementById("pnlfio"+a).style.display="none";
	}
	var firm_obt_ondel=[];
	function delobt(a){
		firm_obt_ondel.push(a);
		//alert(firm_obt_ondel);
		document.getElementById("pnlobt"+a).style.display="none";
	}
/*	function checkLogout(){
		if(firm_profile==1||firm_addresses==1||firm_arrangement==1||firm_business_types==1||firm_disclosure_events==1||firm_do_eo==1||firm_indirectowners==1||firm_operations_summary==1||firm_other_business_types==1||firm_other_names==1||firm_reg_dates==1||firm_sec_reg==1 ){
			var r=confirm(" You didn't save your entries you wish to exit?");
			return r;
			
		}else
			return true;
	}*/
	var hook = true;
  window.onbeforeunload = function() {
    if (hook) {

      return "Did you save your stuff?"
    }
  }
  function unhook() {
    hook=false;
  }
  
  function askforcopy(){
	  var r=confirm(" do you want to use mailing address same as office address?");
	  if(r==true){
		  alert('skn');
		/*  alert(document.getElementById("famlal1").value=document.getElementById("famnal1").value);
		  document.getElementById("famlal2").value=document.getElementById("famnal2").value;
		  document.getElementById("famlal3").value=document.getElementById("famnal3").value;
		  document.getElementById("famlct").value=document.getElementById("famnct").value;
		  document.getElementById("famlst").value=document.getElementById("famnst").value;
		  document.getElementById("famlcn").value=document.getElementById("famncn").value;
		  document.getElementById("famlz").value=document.getElementById("famnz").value;
		 */
		 
		document.getElementById("famlal1").setAttribute('value','abcdef');
		document.getElementById("famlal2").setAttribute('value','asjkf');
	  }
  }
</script>
	<div class="navbarcustom">
		<p class="heading">Hello, <?php echo $name;?></p>
		
		<a href="logout.php" class="logout" >Logout</a>
		<a href="dashboard.php" class="logout" >Dashboard</a>
		<a href="https://files.brokercheck.finra.org/firm/firm_<?php echo $CRD_No;?>.pdf" class="logout" target='_blank'>Pdf Link</a>
	</div>
	
	<p style="position: relative;top:111px;left:50px;font-size:20px;">COMPANY NAME: <span style="color:#5d92ba"><?php echo $row["company_name"];?></span></p>
	<p style="position: relative;top:75px;right:200px;float:right;font-size: 20px;">#CRD NO: <span style="color:#5d92ba"><?php echo $CRD_No;?></span></p>
		<button class="btn btn-basic" type="button" style="position: absolute; top:197px; right:44px; z-index:1;" id="finalsubmit" >SUBMIT FORMS</button>
	<div class="container" style='z-index:0;'>
	<div class="row">
        <div class="col-lg-5 col-md-5 col-sm-8 col-xs-9 bhoechie-tab-container">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
              <div class="list-group" >
                <a href="#" class="list-group-item active text-center">
                 PROFILE
                </a>
                <a href="#" class="list-group-item text-center">
                   OPERATIONS SUMMARY
                </a>
                <a href="#" class="list-group-item text-center">
                 ADDRESSES
                </a>
				<a href="#" class="list-group-item text-center">
                 OTHER NAMES
                </a>
                <a href="#" class="list-group-item text-center">
                 DO EO
                </a>
                <a href="#" class="list-group-item text-center">
				INDIRECT OWNERS
                  
                </a>
				<a href="#" class="list-group-item text-center">
				   SEC REG
                </a>
				<a href="#" class="list-group-item text-center">
				REG DATES
                </a>
				<a href="#" class="list-group-item text-center">
                  BUSINESS TYPES
                </a>
				<a href="#" class="list-group-item text-center">
                  OTHER BUSINESS TYPES
                </a>
				<a href="#" class="list-group-item text-center">
                  ARRANGEMENTS
                </a>
				<a href="#" class="list-group-item text-center">
                AFFLIATION DETAILS
                </a>
				<a href="#" class="list-group-item text-center">
                  DISCLOSURE EVENTS
                </a>
				
				
				
				
              </div>
			</div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
			
			<div class="bhoechie-tab-content active">
							  <form >
							  <button class="btn btn-primary" type="button" style="position:relative;top:10px;float:right;">Edit</button>
							  
							  
                    <div class="form-group" style=" position:relative;width:300px;float:left;margin:20px;">
  <label for="fpfn">firm name:</label>
  <input type="text" class="form-control" id="fpfn">
</div>
<!--<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fpffln">firm full legal name:</label>
  <input type="text" class="form-control" id="fpffln">
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fpfdba">firm doing business as:</label>
  <input type="text" class="form-control" id="fpfdba">
</div>-->
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fpsn">SEC NO:</label>
  <input type="text" class="form-control" id="fpsn">
</div>
<div class="dropdown" style=" position:relative;width:300px; float:left; margin:20px;">
<!-- fpfc-->
  <label for="fpfc">firm classification:</label><br>
   <select id="fpfc" style="width:296px;height:33px;border-radius:5px;">
  <option value="volvo">Volvo</option>
  <option value="saab">Saab</option>
  <option value="mercedes">Mercedes</option>
  <option value="audi">Audi</option>
</select> 
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fpffd">firm founded date:</label>
  <input type="date" class="form-control" id="fpffd">
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fpffl">firm founded location:</label>
  <input type="text" class="form-control" id="fpffl">
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fpffe">Firm_FiscalYear_EndMonth:</label>
  <input type="text" class="form-control" id="fpffe">
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fpbtn">business tel no:</label>
  <input type="text" class="form-control" id="fpbtn">
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fbrb">regulated by:</label>
  <input type="text" class="form-control" id="fprb">
</div>
</form>
</div>
<div class="bhoechie-tab-content "><!-- firm operations summary -->
							  <form>
							  <button class="btn btn-primary" type="button" style="position:relative;top:20px;float:right;">Edit</button><br>
							   
							  <div class="radiowithtext" style="position:relative;top:30px;margin:15px;">
<label style="display:inline-block" >Is REG WITH SEC?</label>
<label class="radio-inline" style="position:relative; float:right;right:300px;"><input type="radio" name="fosirws" value="yes" checked="yes" onkeydown="firm_operations_summary=1;">Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fosirws" value="no" onkeydown="firm_operations_summary=1;">No</label>
</div> 

<div class="radiowithtext" style="position:relative;top:20px;margin:15px;">
<label style="display:inline-block">Is REG WITH SRO?</label>
<label class="radio-inline" style="position:relative; float:right;right:300px;"><input type="radio" name="fosirwso" value="yes" checked="checked" onchange="firm_operations_summary=1;">Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fosirwso" value="no" onchange="firm_operations_summary=1;">No</label>
</div> 
<div class="form-group" style=" position:relative;width:300px; top:-11px;float:left; left:-5px;margin:20px;" id='nosroreg'>
  <label for="fosnsr">NO SRO REG:</label>
  <input type="number" class="form-control" id="fosnsr" onkeydown="firm_operations_summary=1;">
</div>
<div class="radiowithtext" style="position:relative;top:83px;margin:15px;left:-324px;">
<label style="display:inline-block">Is REG WITH STATES TERRITORIES?</label>
<label class="radio-inline" style="position:relative; float:right;right:-25px;"><input type="radio" name="fosirwst" value="yes" checked="checked" onchange="firm_operations_summary=1;">Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:-121px;"><input type="radio" name="fosirwst" value="no" onchange="firm_operations_summary=1;">No</label>
</div> 

<div class="form-group" style=" position:relative;width:300px; top:53px;float:left; left:-344px;margin:20px;">
  <label for="fosnstr">NO STATES TERRITORIES REG:</label>
  <input type="number" class="form-control" id="fosnstr" onkeydown="firm_operations_summary=1;">
</div><br><br><br><br><br>
<div class="radiowithtext" style="position:relative;margin:15px;top:28px;">
<label style="display:inline-block">IS CURRENTLY SUSPENDED?</label>
<label class="radio-inline" style="position:relative; float:right;right:300px;"><input type="radio" name="fosics" value="yes" onchange="firm_operations_summary=1;">Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fosics" value="no" checked="checked" onchange="firm_operations_summary=1;">No</label>
</div> 
<div class="radiowithtext" style="position:relative;margin:15px;top:29px;">
<label style="display:inline-block">IS AFFI FIN INVST INSTITUTIONS?</label>
<label class="radio-inline" style="position:relative; float:right;right:300px;"><input type="radio" name="fosiafii" value="yes" onchange="firm_operations_summary=1;">Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fosiafii" value="no" onchange="firm_operations_summary=1;">No</label>
</div> 
<div class="radiowithtext" style="position:relative;margin:15px;top:28px;">
<label style="display:inline-block">HAS REF ARR OTHER BD?</label>
<label class="radio-inline" style="position:relative; float:right;right:300px;"><input type="radio" name="foshraob" value="yes" onchange="firm_operations_summary=1;">Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="foshraob" value="no" onchange="firm_operations_summary=1;">No</label>
</div> 
<div class="form-group" style=" position:relative;width:300px; top:5px;float:left; left:-5px;margin:20px;">
  <label for="fosntb">NO TYPE BUSINESSES:</label>
  <input type="number" class="form-control" id="fosntb" onkeydown="firm_operations_summary=1;" >
</div>
</form>
</div>

                  <div class="bhoechie-tab-content "><!--firm addresses -->
				  <button class="btn btn-primary" type="button" style="position:relative;top:10px;float:right;">EDIT</button>
				 				 
								 <form>
			<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fpffln">firm full legal name:</label>
  <input type="text" class="form-control" id="fpffln" onkeydown="firm_profile=1;">
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fpfdba">firm doing business as:</label>
  <input type="text" class="form-control" id="fpfdba" onkeydown="firm_profile=1;">
</div>	
			<div style='max-width:400px;float:left;'>	<center>  <p style='font-size:20px;color:#5d92ba;'><b><i>MAIN OFFICE ADDRESS</i></b></p></center>
				
                   

<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="faali">Address Line1:</label>
  <input type="text" class="form-control" id="famnal1" onkeydown="firm_addresses=1;">
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="faal2">Address Line2:</label>
  <input type="text" class="form-control" id="famnal2" onkeydown="firm_addresses=1;">
</div>

<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="faal3">Address Line3:</label>
  <input type="text" class="form-control" id="famnal3" onkeydown="firm_addresses=1;">
</div>
 <div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="sel1">CITY:</label>
  <select class="form-control" id="famnct" onkeydown="firm_addresses=1;">
    <option>1</option>
    <option>2</option>
    <option>3</option>
    <option>4</option>
  </select>
</div> 

<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="sel1">STATE:</label>
  <select class="form-control" id="famnst" onkeydown="firm_addresses=1;">
    <option>1</option>
    <option>2</option>
    <option>3</option>
    <option>4</option>
  </select>
</div> 

<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fac">Country:</label>
  <select class="form-control" id="famncn" onkeydown="firm_addresses=1;">
    <option>1</option>
    <option>2</option>
    <option>3</option>
    <option>4</option>
  </select>
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="faz">Zip Code:</label>
  <input type="text" class="form-control" id="famnz" onkeydown="firm_addresses=1;">
</div>
</form>
</div>
<br>
<br>
<form>
 <div class="mailofficediv" style="max-width:400px;float:left;position:relative;top:0px;">
<center>  <p style='font-size:20px;color:#5d92ba;'><b><i>MAILING OFFICE ADDRESS</i></b></p></center>
                   
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="faali">Address Line1:</label>
  <input type="text" class="form-control" id="famkal1" onkeydown="firm_addresses=1;">
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="faal2">Address Line2:</label>
  <input type="text" class="form-control" id="famlal2" onkeydown="firm_addresses=1;">
</div>

<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="faal3">Address Line3:</label>
  <input type="text" class="form-control" id="famlal3" onkeydown="firm_addresses=1;">
</div>
 <div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="sel1">CITY:</label>
  <select class="form-control" id="famlct" onkeydown="firm_addresses=1;">
    <option>1</option>
    <option>2</option>
    <option>3</option>
    <option>4</option>
  </select>
</div> 

<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="sel1">STATE:</label>
  <select class="form-control" id="famlst" onkeydown="firm_addresses=1;">
    <option>1</option>
    <option>2</option>
    <option>3</option>
    <option>4</option>
  </select>
</div> 

<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fac">Country:</label><!-- famlcn-->
 <select class="form-control" id="famlcn" onkeydown="firm_addresses=1;">
    <option>1</option>
    <option>2</option>
    <option>3</option>
    <option>4</option>
  </select>
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="faz">Zip Code:</label>
  <input type="text" class="form-control" id="famlz" onkeydown="firm_address=1;">
</div>
</div>
</form>
</div>
<div class="bhoechie-tab-content"><!-- firm other names-->
 <center>
				  <button class="btn btn-info" type="button" id="add_firmon">Add Entry</button>
				  
				  <div id="add_firmon_addhere" class="panel-group" style="margin-top:20px;">
				  
				  </div>
				  </center>
				  
				  
                  <!--  <div class="form-group" style=" position:relative;width:300px;float:left;margin:20px;">
  <label for="fonon">OTHER NAME:</label>
  <input type="text" class="form-control" id="usr">
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fonsl">STATES LIST:</label>
  <input type="text" class="form-control" id="usr">
</div>-->




					</div>
	<div class="bhoechie-tab-content"><!-- firm_do_eo -->
								<center>
				<button class="btn btn-info" type="button" id="firm_do_eo_btn" >Add Entry</button>
				</center>
	<div id="fde_add_here" class='panel-group' style="margin-top:20px;">
	
				
</div>
</div>

<div class="bhoechie-tab-content">

				<center>
				<button class="btn btn-info" type="button" id="firm_indirectowners_btn" >Add Entry</button>
				</center>
					<div id="fio_add_here" class="panel-group" style='margin-top:20px;'>
					
					
	</div>
</div>
<div class="bhoechie-tab-content ">
<form>
<button class="btn btn-primary" type="button" style="position:relative;top:10px;float:right;">Edit</button>
<br>
<div class="radiowithtext" style="position:relative;top:100px; margin:15px;">
<label style="display:inline-block">Is BD?</label>
<label class="radio-inline" style="position:relative; float:right;right:300px;"><input type="radio" name="fsrib" value="yes" onclick="firm_sec_reg=1">Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fsrib" value="no" onclick="firm_seg_reg=1;">No</label>
</div> 
<div class="radiowithtext" style="position:relative;top:100px; margin:15px;">
<label style="display:inline-block">Is BD AND GSBD?</label>
<label class="radio-inline" style="position:relative; float:right;right:300px;"><input type="radio" name="fsricag" value="yes" onclick="firm_seg_reg=1;">Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fsricag" value="no" onclick="firm_seg_reg=1;">No</label>
</div> 
<div class="radiowithtext" style="position:relative;top:100px; margin:15px;">
<label style="display:inline-block">Is GSBD ONLY?</label>
<label class="radio-inline" style="position:relative; float:right;right:300px;"><input type="radio" name="fsigo" value="yes" onclick="firm_sec_reg=1;">Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fsigo" value="no" onclick="firm_seg_reg=1;">No</label>
</div> 
<div class="radiowithtext" style="position:relative;top:100px;margin:15px;">
<label style="display:inline-block">Is GSBD CEASED?</label>
<label class="radio-inline" style="position:relative; float:right;right:300px;"><input type="radio" name="fsigc" value="yes" onclick="firm_sec_reg=1;">Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fsigc" value="no" onclick="firm_sec_reg=1;">No</label>
</div> 
</form>
</div>
<div class="bhoechie-tab-content">
<button class="btn btn-primary" type="button" style="position:relative;top:10px;float:right;">Edit</button>
<form>
							  
<label>SEC </label><br><br>
<label class="radio-inline" ><input type="radio" name="frdsa" value="approved">Approved</label>
<label class="radio-inline" style="margin-right:20px;"><input type="radio" name="frdsa" value="not approved">Not approved</label>
<input type="text" placeholder="date" id="secdate" onkeydown='firm_reg_dates=1;'/>
<br><br><br>
<label>SRO</label><br><br>
<textarea cols="50" rows="10" id="srotf" onkeydown='firm_reg_dates=1;'></textarea>
<br><br><br>
<label>US STATES AND TERRITORIES</label><br><br>
<textarea cols="50" rows="10" id="uster" onkeydown='firm_reg_dates=1;'></textarea>
</form>
</div>
<div class="bhoechie-tab-content ">
							   <form>
							   <button class="btn btn-primary" type="button" style="position:relative;float:right;">Edit</button><br><br><br>
							  
							   
                
<!-- business types-->
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Exchange member engaged in exchange commission business other than floor activities
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtiecm" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtiecm" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">"Exchange member engaged in floor activities
in floor activities "
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtiefa" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtiefa" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Broker or dealer making inter-dealer markets in corporate securities over-the-counter
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtibico" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtibico" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Broker or dealer retailing corporate equity securities over-the-counter
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtibrco" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtibrco" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Broker or dealer selling corporate debt securities
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtibscs" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtibscs" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Underwriter or selling group participant (corporate securities other than mutual funds)
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtisuse" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtisuse" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Mutual fund underwriter or sponsor
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtiusm" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtiusm" onclick='firm_business_types=1;' checked='checked'>No</label>
</div><div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Mutual fund retailer
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtimr" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtimr" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">U.S. government securities dealer
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtidu" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtidu" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">U.S. government securities broker
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtibu" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtibu" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Municipal securities broker
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtidm" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtidm" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Municipal securities broker
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtibm" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtibm" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Broker or dealer selling variable life insurance or annuities
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtisbsva" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtisbsva" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Solicitor of time deposits in a financial institution
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtist" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtist" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block"> Real estate syndicator
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtirs" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtirs" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Broker or dealer selling oil and gas interests
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtibso" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtibso" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Put and call broker or dealer or option writer
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtibo" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtibo" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Broker or dealer selling securities of only one issuer or<br/> associate issuers (other than mutual funds)
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtibss" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtibss" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Broker or dealer selling securities of non-profit organizations (e.g., churches, hospitals)
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtibsn" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtibsn" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Investment advisory services
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtiias" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtiias" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Broker or dealer selling tax shelters or limited partnerships in primary distributions
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtibstlp" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtibstlp" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Broker or dealer selling tax shelters or limited partnerships in the secondary market
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtibstls" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtibstls" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Non-exchange member arranging for transactions in listed securities by exchange member
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtinal" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtinal" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Trading securities for own account
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtipt" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtipt" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Private placements of securities
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtipps" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtipps" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Broker or dealer selling interests in mortgages or other receivables
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtibsm" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtibsm" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Broker or dealer involved in a networking, kiosk or similar arrangement with bank,<br/> savings bank or association, or credit union
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtibnb" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtibnb" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Broker or dealer involved in a networking, kiosk or<br/> similar arrangement with insurance company or agency
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtibni" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtibni" onclick='firm_business_types=1;' checked='checked'>No</label>
</div>
<div class="radiowithtext" style="position:relative;top:20px;margin:10px;">
<label style="display:inline-block">Other
</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fbtiob" onclick='firm_business_types=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:100px;"><input type="radio" name="fbtiob" onclick='firm_business_types=1;' checked='checked'>No</label>
</div></form>
</div>

                 <div class="bhoechie-tab-content">
                 <center>
				 <button class="btn btn-info" type="button" id="fobt_add_btn"> ADD FIELD</button>
		
				 </center>
				 <div id="fobt_add_here">
				 </div>

                </div>
				 <div class="bhoechie-tab-content">
				 <button class="btn btn-primary" type="button" style="position:relative;float:right;">Edit</button><br>
							 
				 <div class="radiowithtext" style="position:relative;top:100px;margin:15px;">
<label style="display:inline-block">hold or maintain funds or securities or provide clearing services for other broker-dealer(s)</label>
<label class="radio-inline" style="position:relative; float:right;right:128px;"><input type="radio" name="faicab" value="yes" onclick='firm_arrangement=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:32px;"><input type="radio" name="faicab" value="no" onclick='firm_arrangement=1;' checked='checked'>No</label>
</div> 
<div class="radiowithtext" style="position:relative;top:100px;margin:15px;">
<label style="display:inline-block">hold or maintain funds or securities or provide clearing services for other broker-dealer(s)refer or introduce customers to other brokers and dealers</label>
<label class="radio-inline" style="position:relative; float:right;right:127px; top:-19px"><input type="radio" name="faiiao" value="yes" onclick='firm_arrangement=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:27px;top:-19px;"><input type="radio" name="faiiao" value="no" onclick='firm_arrangement=1;' checked='checked'>No</label>
</div> 
<div class="radiowithtext" style="position:relative;top:100px;margin:15px;">
<label style="display:inline-block">does have books or records maintained by a third party</label>
<label class="radio-inline" style="position:relative; float:right;right:37px;"><input type="radio" name="faabbtp" value="yes" onclick='firm_arrangement=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:-66px;"><input type="radio" name="faabbtp" value="no" onclick='firm_arrangement=1;' checked='checked'>No</label>
</div> 
<div class="radiowithtext" style="position:relative;top:100px;margin:15px;">
<label style="display:inline-block">have accounts, funds, or securities maintained by a third party</label>
<label class="radio-inline" style="position:relative; float:right;right:126px;"><input type="radio" name="faaabt" value="yes" onclick='firm_arrangement=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:24px;"><input type="radio" name="faaabt" value="no" onclick='firm_arrangement=1;' checked='checked'>No</label>
</div> 
<div class="radiowithtext" style="position:relative;top:100px;margin:15px;">
<label style="display:inline-block">customer accounts, funds, or securities maintained by a third party</label>
<label class="radio-inline" style="position:relative; float:right;right:125px;"><input type="radio" name="faacbtp" value="yes" onclick='firm_arrangement=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:24px;"><input type="radio" name="faacbtp" value="no" onclick='firm_arrangement=1;' checked='checked'>No</label>
</div> 
<div class="radiowithtext" style="position:relative;top:100px;margin:15px;">
<label style="display:inline-block">Is_Indv_Control_Mngt_policies?</label>
<label class="radio-inline" style="position:relative; float:right;right:127px;"><input type="radio" name="faiicmp" value="yes" onclick='firm_arrangement=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:24px;"><input type="radio" name="faiicmp" value="no" onclick='firm_arrangement=1;' checked='checked'>No</label>
</div> 
<div class="radiowithtext" style="position:relative;top:100px;margin:15px;">
<label style="display:inline-block">Is_Indv_Finance_FirmBusiness?</label>
<label class="radio-inline" style="position:relative; float:right;right:126px;"><input type="radio" name="faiiff" value="yes" onclick='firm_arrangement=1;'>Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:23px;"><input type="radio" name="faiiff" value="no" onclick='firm_arrangement=1;' checked='checked'>No</label>=
</div> 
</div>
  <div class="bhoechie-tab-content">
				  <form name="firm_affliation_dtls">
				  <button class="btn btn-primary" type="button" style="position:relative;top:10px;float:right;">Edit</button>
				               
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fadpc">AFFLIATION COMPANY CRD NO:</label>
  <input type="text" class="form-control" id="fadpc" onkeydown='firm_affliationdtls=1;'>
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fadn">AFFLIATION COMPANY NAME:</label>
  <input type="text" class="form-control" id="fadn" onkeydown='firm_affliationdtls=1;'>
</div>

<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">

  <label for="fadty">TYPE:</label>
  <select class="form-control" id="fadty">
    <option>1</option>
    <option>2</option>
    <option>3</option>
    <option>4</option>
  </select>
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fadba">BUSINESS ADDRESS:</label>
  
  <input type="text" class="form-control" id="fadba" style="height:85px;" onkeydown='firm_affliationdtls=1;'>
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="faded">EFFECTIVE DATE:</label>
  <input type="date" class="form-control" id="faded" placeholder="Format for date is yyyy/mm/dd:" onkeydown='firm_affliationdtls=1;'>
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;" >
  <label for="fadde">DESCRIPTION:</label>
  <input type="text" class="form-control" id="fadde" style="height:85px;" onkeydown='firm_affliationdtls=1;'>
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;" >
  <label for="fadfe">FOREIGN ENTITY:</label>
  <input type="text" class="form-control" id="fadfe" onkeydown='firm_affliationdtls=1;'>
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fadco">COUNTRY:</label>
  <input type="text" class="form-control" id="fadco" onkeydown='firm_affliationdtls=1;'>
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fadsa">SECURITY ACTIVITIES:</label>
  <input type="text" class="form-control" id="fadsa" onkeydown='firm_affliationdtls=1;'>
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="fadid">INVESTMENT ADVISORY:</label>
  <input type="text" class="form-control" id="fadid" onkeydown='firm_affliationdtls=1;'>
</div>

</form>
    </div>
	 <div class="bhoechie-tab-content">
				
				
				<div style="position:relative;top:50px;">
				<label style="display:inline-block">Are events disclosed?</label>
<label class="radio-inline" style="position:relative; float:right;right:300px;"><input type="radio" name="fdeaed" value="yes" onclick="$('.btn.btn-info').css('display','block'); firm_disclosure_events=0;">Yes</label>
<label class="radio-inline" style="position:relative; float:right;right:200px;"><input type="radio" name="fdeaed" value="no" onclick="firm_disclosure_events=1;" >No</label>
</div>

				<center>
				<button class="btn btn-info" type="button" style="display:none; position:relative;top:80px;" id="disclosure_btn">Add Entry</button>
				</center>
				
				<div id="reg_discevents_add_here" style='margin-top:100px' class='panel-group'>
				 

				</div>
				
                 
               <!--   <div class="form-group" style=" position:relative;width:300px;float:left;margin:20px;">	
 <label for="frdrsn">REG_SEQ_NO:</label>
  <input type="text" class="form-control" id="frdrsn" >
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="frdrtn">REG_TYPE_NAME:</label>
  <input type="text" class="form-control" id="frdrtn" >
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="frdrbn">REG_BODY_NAME:</label>
  <input type="text" class="form-control" id="frdrbn">
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="frdrs">REG_STATUS:</label>
  <input type="text" class="form-control" id="frdrs">
</div>
<div class="form-group" style=" position:relative;width:300px; float:left; margin:20px;">
  <label for="frdred">REG_EFFDATE:</label>
  <input type="text" class="form-control" id="frdred">
</div>-->
</form>

                </div>

 
                <!-- flight section -->
				
               
                <!-- train section -->
			









				 
                

                
                              
                            
                               
                             
                            
                              
                             

</div>
</div>
</div>
</div>
<div class="footer">
</div>
</body>
</html>