<?php
session_start();
$conn=mysqli_connect("localhost","root","","bfsi");
if(!$conn){
	echo "Problem with connection";
}
$CRD_No=$_GET["crdno"];
$id=$_SESSION["assid"];
$name=$_SESSION["name"];
$getcom="select company_name from bd_master where CRD_No='$CRD_No'";
$res=mysqli_query($conn,$getcom);
$row=mysqli_fetch_assoc($res);
?>
<html>
<head>

	<title>FORM</title>
	 <meta name="viewport" content="width=device-width, initial-scale=1">

      <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/dashboard.css"/>
</head>
<body>
<script>
var editing_current;
var firm_addresses=0;
var firm_affliationdtls=0;
var firm_arrangement=0;
var firm_business_types=0;
var firm_disclosure_events=0;
var firm_do_eo=0;
var firm_indirectowners=0;
var firm_operations_summary=0;
var firm_other_names=0;
var firm_profile=0;
var firm_reg_dates=0;
var firm_sec_reg=0;
$(document).ready(function(){
	$('.btn.btn-info').click(function(){
		$('.btn').css('display','none');
		$('.btn.btn-success').css('display','block');
		 editing_current=1;
	});
	
	$('.tabs').click(function(){
		if(editing_current==1){
			alert("you didn't save previous forms data");
			$('.btn').css('display','inline-block');
			$('.btn.btn-success').css('display','none');
			editing_current=0;
		}else{
			$('.btn').css('display','inline-block');
			$('.btn.btn-success').css('display','none');
		}
	});
	
	$('#finalsubmit').click(function(){
		//alert(firm_addresses+""+firm_affliationdtls+""+firm_arrangement+""+firm_business_types+""+firm_disclosure_events+""+firm_do_eo+""+firm_indirectowners+
		//""+firm_operations_summary+""+firm_other_names+""+firm_profile+""+ firm_reg_dates +""+firm_sec_reg);
		
	// submitted forms which he wanted to edit
	
	if(firm_addresses!=0){ //this indicates the save for firm_addresses is clicked
		var crd=$("#facrd").val();
		var addid=$("#faai").val();
		var addtype=$("#faat").val();
		var addline1=$("#faali").val();
		var addline2=$("#faal2").val();
		var addline3=$("#faal3").val();
		var city=$("#fac").val();
		var state=$("#fas").val();
		var country=$("#fac").val();
		var zipcode=$("#faz").val();
		//alert(crd+""+addid+""+addtype+""+addline1+""+addline2+""+addline3+""+city+""+state+""+country+""+zipcode);
		var url="insert into firm_addresses values('"+crd+"','12','2017','"+addid+"','"+addtype+"','"+addline1+"','"+addline2+"','"+addline3+"','"+city+"','"+state+"','"+country+"','"+zipcode+"');";
		$.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
		 
	}
	if(firm_affliationdtls!=0){
		var bdname=$("#fadbdn").val();
		var pdflink=$("#fadpdl").val();
		var pdfcrd=$("#fadpc").val();
		var name=$("#fadn").val();
		var crdno=$("#fadcrd").val();
		var type=$("#fadty").val();
		var businessadd=$("#fadba").val();
		var effdate=$("#faded").val();
		var desc=$("#fadde").val();
		var forent=$("#fadfe").val();
		var cntry=$("#fadco").val();
		var secact=$("#fadsa").val();
		var inveadv=$("#fadid").val();
		var url="insert into firm_affliationdtls values('"+bdname+"','"+pdflink+"','"+pdfcrd+"','"+name+"','"+crdno+"','"+type+"','12','2017','"+businessadd+"','"+effdate+"','"+desc+"','"+forent+"','"+cntry+"','"+secact+"','"+inveadv+"');";
		$.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
	}
	if(firm_arrangement!=0){
		var crdnonew=$("#farrcrd").val();
		var icaob = $("input[name='farrcaob']:checked").val();
		var iiaob = $("input[name='farriiaob']:checked").val();
		var abrbtp=$("input[name='farrabrbtp']:checked").val();
		var aafsftp = $("input[name='farraafsftp']:checked").val();
		var acafsftp = $("input[name='farracafsftp']:checked").val();
		var iicmp=$("input[name='farriiffb']:checked").val();
		var iiffb = $("input[name='farriiffb']:checked").val();
		var url="insert into firm_arrangements values('"+crdnonew+"','"+icaob+"','"+iiaob+"','"+abrbtp+"','"+aafsftp+"','"+acafsftp+"','"+iicmp+"','"+iiffb+"','12','2017');";
		$.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
	}
	
	if(firm_business_types!=0){//single entry problem look at it in the morning
		var fbtcrdno=$("#fbtcrd").val();
		var iecb=$("input[name='fbtiecm']:checked").val();
		var iefa=$("input[name='fbtiefa']:checked").val();
		var ibico=$("input[name='fbtibico']:checked").val();
		var ibrco=$("input[name='fbtibrco']:checked").val();
		var ibscs=$("input[name='fbtibscs']:checked").val();
		var iuse=$("input[name='fbtisuse']:checked").val();
		var iusm=$("input[name='fbtiusm']:checked").val();
		var imr=$("input[name='fbtimr']:checked").val();
		var idu=$("input[name='fbtidu']:checked").val();
		var ibu=$("input[name='fbtibu']:checked").val();
		var idm=$("input[name='fbtidm']:checked").val();
		var ibm=$("input[name='fbtibm']:checked").val();
		var ibsva=$("input[name='fbtisbsva']:checked").val();
		var ist=$("input[name='fbtist']:checked").val();
		var irs=$("input[name='fbtirs']:checked").val();
		var ibso=$("input[name='fbtibso']:checked").val();
		var ibo=$("input[name='fbtibo']:checked").val();
		var ibss=$("input[name='fbtibss']:checked").val();
		var ibsn=$("input[name='fbtibsn']:checked").val();
		var iias=$("input[name='fbtiias']:checked").val();
		var ibstls=$("input[name='fbtibstls']:checked").val();
		var inal=$("input[name='fbtinal']:checked").val();
		var ipto=$("input[name='fbtipt']:checked").val();
		var ips=$("input[name='fbtipps']:checked").val();
		var ibsm=$("input[name='fbtibsm']:checked").val();
		var ibnb=$("input[name='fbtibnb']:checked").val();
		var ibni=$("input[name='fbtibni']:checked").val();
		var iob=$("input[name='fbtiob']:checked").val();
		
		var url="insert into firm_business_types values('"+fbtcrdno+"','12','2017','"+iecb+"','"+iefa+"','"+ibico+"','"+ibrco+"','"+ibscs+"','"+iuse+"','"+iusm+"','"+imr+"','"+idu+"','"+ibu+"','"+idm+"','"+ibm+"','"+ibsva+"','"+ist+"','"+irs+"','"+ibso+"','"+ibo+"','"+ibss+"','"+ibsn+"','"+iias+"','"+ibstls+"','"+inal+"','"+ipto+"','"+ips+"','"+ibsm+"','"+ibnb+"','"+ibni+"','"+iob+"');";
	    $.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
	}
	if(firm_disclosure_events!=0){
		var fdecrd=$("#fdecrd").val();
		var fdeaed=$("input[name='fdeaed']:checked").val(); 
        var fdertne=$("#fdertne").val();	
        var url="insert into firm_disclosure_events values('"+fdecrd+"','"+fdeaed+"','"+fdertne+"','12','2017');";	
$.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });		
	}
	if(firm_do_eo!=0){
		var crdno=$("#fdecrdno").val();
		var den=$("#fdeden").val();
		var deln=$("#fdedeln").val();
		var decn=$("#fdedecn").val();
		var det=$("#fdedet").val();
		var p=$("#fdedep").val();
		var psd=$("#fdepsd").val();
		var po=$("#fdepo").val();
		var ddmp=$("input[name='fdeddmp']:checked").val();
		var iprc=$("input[name='fdeiprc']:checked").val();
		var url="insert into firm_do_eo values('"+crdno+"','"+den+"','12','2017','"+deln+"','"+decn+"','"+det+"','"+p+"','"+psd+"','"+po+"','"+ddmp+"','"+iprc+"');";
	    $.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });	
	}
	if(firm_indirectowners!=0){
		var crdno=$("#fiocrd").val();
		var idono=$("#fioido").val();
		var iln=$("#fioiln").val();
		var icn=$("#fioicn").val();
		var it=$("#fioit").val();
		var iv=$("#fioiv").val();
		var rtd=$("#fiortd").val();
		var red=$("#fiored").val();
		var po=$("#fiopo").val();
		var ddmp=$("input[name='fioddmp']:checked").val();
		var prc=$("input[name='fioiprc']:checked").val();
		var url="insert into firm_indirectowners values('"+crdno+"','"+idono+"','"+iln+"','"+icn+"','12','2017','"+it+"','"+iv+"','"+rtd+"','"+red+"','"+po+"','"+ddmp+"','"+prc+"');";
		 $.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
	}
	if(firm_operations_summary!=0){
	  var foscrd=$("#foscrd").val();
	  var irws=$("input[name='fosirws']:checked").val();
	  var irwsr=$("input[name='fosirwso']:checked").val();
	  var iswst=$("input[name='fosirwst']:checked").val();
	  var nsr=$("#fosnsr").val();
	  var nstr=$("#fosnstr").val();
	  var ics=$("input[name='fosics']:checked").val();
	  var fosntb=$("#fosntb").val();
	  var iadii=$("input[name='fosiafii']:checked").val();
	  var hraob=$("input[name='foshraob']:checked").val();
	  var url="insert into firm_operations_summary values('"+foscrd+"','"+irws+"','"+irwsr+"','"+iswst+"','"+nsr+"','"+nstr+"','12','2017','"+ics+"','"+fosntb+"','"+iadii+"','"+hraob+"');";
	  $.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
	}
	if(firm_other_names!=0){
		var foncrd=$("#foncrd").val();
		var fonon=$("#fonon").val();
		var fonsl=$("#fonsl").val();
		var url="insert into firm_other_names values('"+foncrd+"','"+fonon+"','"+fonsl+"','12','2017');";
		 $.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
	}
	if(firm_profile!=0){
		var fn=$("#fpfn").val();
		var ffln=$("#fpffln").val();
		var fdba=$("#fpfdba").val();
		var crdno=$("#fpcn").val();
		var secno=$("#fpsn").val();
		var fc=$("#fpfc").val();
		var ffd=$("#fpffd").val();
		var ffl=$("#fpffl").val();
		var ffye=$("#fpffe").val();
		var ffbtl=$("#fpbtn").val();
		var rb=$("#fprb").val();
		var url="insert into firm_profile values('"+fn+"','"+ffln+"','"+fdba+"','"+crdno+"','"+secno+"','12','2017','"+fc+"','"+ffd+"','"+ffl+"','"+ffye+"','"+ffbtl+"','"+rb+"');";
		$.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
	}
	if(firm_reg_dates!=0){
		var frcrd=$("#frdcrd").val();
		var frdrsn=$("#frdrsn").val();
		var frdrtn=$("#frdrtn").val();
		var frdrbn=$("#frdrbn").val();
		var frdrs=$("#frdrs").val();
		var frdred=$("#frdred").val();
		var url="insert into firm_reg_dates values('"+frcrd+"','"+frdrsn+"','"+frdrtn+"','"+frdrbn+"','"+frdrs+"','"+frdred+"','12','2017');"
		$.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
	}
	if(firm_sec_reg!=0){
		var crdno=$("#fsrcn").val();
		var fsrib=$("input[name='fsrib']:checked").val();
		var fsricag=$("input[name='fsricag']:checked").val();
		var fsigo=$("input[name='fsigo']:checked").val();
		var fsigc=$("input[name='fsigc']:checked").val();
		var url="insert into firm_sec_reg values('"+crdno+"','"+fsrib+"','"+fsricag+"','"+fsigo+"','"+fsigc+"','12','2017');";
		$.ajax({
         type: "post",
		 data:{url:url},
         url: "insert.php",
         success: function(data){
              alert("Data Save: " + data);
         }
		 });
		
	}
	
	
	});
	
});

		
		
	
</script>
	<div class="navbarcustom">
		<p class="heading">Hello, <?php echo $name;?></p>
		<a href="logout.php" class="logout">Logout</a>
		<a href="dashboard.php" class="logout">Dashboard</a>
	</div>
	<p style="position: relative;top:111px;left:50px;font-size:20px;">COMPANY NAME: <span style="color:#5d92ba"><?php echo $row["company_name"];?></span></p>
	<p style="position: relative;top:75px;right:200px;float:right;font-size: 20px;">#CRD NO: <span style="color:#5d92ba"><?php echo $CRD_No;?></span></p>
	<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home" onclick="formchanged()" class="tabs">1</a></li>
    <li><a data-toggle="tab" href="#menu1" class="tabs">2</a></li>
    <li><a data-toggle="tab" href="#menu2" class="tabs">3</a></li>
    <li><a data-toggle="tab" href="#menu3"  class="tabs">4</a></li>
    <li><a data-toggle="tab" href="#menu4" class="tabs">5</a></li>
    <li><a data-toggle="tab" href="#menu5" class="tabs">6</a></li>
    <li><a data-toggle="tab" href="#menu6" class="tabs">7</a></li>
    <li><a data-toggle="tab" href="#menu7" class="tabs">8</a></li>
    <li><a data-toggle="tab" href="#menu8" class="tabs">9</a></li>
    <li><a data-toggle="tab" href="#menu9" class="tabs">10</a></li>
    <li><a data-toggle="tab" href="#menu10" class="tabs">11</a></li>
    <li><a data-toggle="tab" href="#menu11" class="tabs">12</a></li>

  </ul>

  <div class="tab-content">
  
    <div id="home" class="tab-pane fade in active" >
    	<center>
    		<p style="color: #5d92ba; font-size: 21px; padding:15px;">FIRM REG DATES</p>
        <form >
        	<div class="form-group">
        		<input type="number" class="form-control" id="frdcrd" style="width:50%;margin:10px;" placeholder="CRDNO"><br>
        		<input type="number" class="form-control" id="frdrsn" style="width:50%;margin:10px;" placeholder="REG_SEQ_NO"><br>
        		<input type="text" class="form-control" id="frdrtn" style="width:50%;margin:10px;" placeholder="REG_TYPE_NAME"><br>
        		<input type="text" class="form-control" id="frdrbn" style="width:50%;margin:10px;" placeholder="REG_BODY_NAME"><br>
        		<input type="text" class="form-control" id="frdrs" style="width:50%;margin:10px;" placeholder="REG_STATUS"><br>
				<input type="text" class="form-control" id="frdred" style="width:50%;margin:10px;" placeholder="REG_EFFDATE"><br>
        		<button class="btn btn-info" type="button" style="position:relative;left:-100px;">EDIT</button>
<button class="btn btn-success" type="button" style="display:none;" onclick="firm_reg_dates=1; editing_current=0;">SAVE</button>
<button class="btn btn-default" type="button" style="position: relative;left: 100px; ">NEXT</button>
        	</div>
        </form>
    </center>
    </div>
    <div id="menu1" class="tab-pane fade">
    	<center>
<p style="color: #5d92ba;padding:15px; font-size: 21px;">FIRM DISCLOSURE EVENTS</p>
     <form style="margin-top: 50px;">
     	<div class="form-group">
 <input type="number" class="form-control" id="fdecrd" style="width:50%;margin:10px;" placeholder="CRDNO"><br>
 <p style="float: left; position: relative;left:344px;">Are events disclosed?</p>
 <input type="radio" name="fdeaed" value="yes"/>yes
<input type="radio" name="fdeaed" value="no"/>no<br><br>
<input type="number" class="form-control" id="fdertne" style="width:50%;margin:10px;" placeholder="Reg type number of events"><br><br>
<button class="btn btn-info" style="position:relative;left:-100px;" type="button">EDIT</button>
<button class="btn btn-success" type="button" style="display:none;" onclick="firm_disclosure_events=1; editing_current=0;">SAVE</button>
<button class="btn btn-default" style="position: relative;left: 100px;" type="button">NEXT</button>

</div>
     </form>
 </center>
    </div>
    <div id="menu2" class="tab-pane fade">
    	<center>
    		<p style="color: #5d92ba;padding:15px; font-size: 21px;">FIRM OTHER NAMES</p>
  <form>
  	<div class="form-group">
  		<input type="number" class="form-control" id="foncrd" style="width:50%;margin:10px;" placeholder="CRDNO"><br>
  		 <input type="text" class="form-control" id="fonon" style="width:50%;margin:10px;" placeholder="OTHER NAMES"><br>
  		  <input type="text" class="form-control" id="fonsl" style="width:50%;margin:10px;" placeholder="STATES LIST"><br>
  		  <button class="btn btn-info" style="position:relative;left:-100px;" type="button">EDIT</button>
<button class="btn btn-success" type="button" style="display:none;" onclick="firm_other_names=1; editing_current=0;">SAVE</button>
<button class="btn btn-default" style="position: relative;left: 100px;" type="button">NEXT</button>
  	</div>
  </form>
</center>
    </div>
    <div id="menu3" class="tab-pane fade">
    	<center>
    		<p style="color: #5d92ba;padding:15px; font-size: 21px;">FIRM ADDRESSES</p>
     <form>
     	<div class="form-group">
     		<input type="number" class="form-control" style="width:50%;margin:10px;" placeholder="CRDNO" id="facrd"><br>
     		<input type="number" class="form-control" style="width:50%;margin:10px;" placeholder="ADDRESS ID" id="faai"><br>
     		<input type="text" class="form-control" style="width:50%;margin:10px;" placeholder="ADDRESS TYPE" id="faat"><br>
     		<input type="text" class="form-control" style="width:50%;margin:10px;" placeholder="ADDRESS LINE 1" id="faali"><br>
     		<input type="text" class="form-control" style="width:50%;margin:10px;" placeholder="ADDRESS LINE 2" id="faal2"><br>
     		<input type="text" class="form-control" style="width:50%;margin:10px;" placeholder="ADDRESS LINE 3" id="faal3"><br>
     		<input type="text" class="form-control" style="width:50%;margin:10px;" placeholder="CITY" id="fac"><br>
     		<input type="text" class="form-control" style="width:50%;margin:10px;" placeholder="STATE" id="fas"><br>
     		<input type="text" class="form-control" style="width:50%;margin:10px;" placeholder="COUNTRY" id="fac"><br>
     		<input type="number" class="form-control" style="width:50%;margin:10px;" placeholder="ZIPCODE" id="faz"><br>
     		<button class="btn btn-info" style="position:relative;left:-100px;" type="button">EDIT</button>
<button class="btn btn-success" type="button" style="display:none;" onclick="firm_addresses=1; editing_current=0;">SAVE</button>
<button class="btn btn-default" style="position: relative;left: 100px;" type="button">NEXT</button>
     		

     	</div>
     </form>
 </center>
    </div>
    <div id="menu4" class="tab-pane fade">
    	<center>
    		<p style="color: #5d92ba;padding:15px; font-size: 21px;">FIRM AFFLIATION DETAILS</p>
    	<form>
    		<div class="form-group">
    			<input type="text" class="form-control" id="fadbdn" style="width:50%;margin:10px;" placeholder="BD NAME"><br>
    			<input type="text" class="form-control" id="fadpdl" style="width:50%;margin:10px;" placeholder="PDF LINK"><br>
    			<input type="number" class="form-control" id="fadpc" style="width:50%;margin:10px;" placeholder="PDF CRD NO"><br>
    			<input type="text" class="form-control" id="fadn" style="width:50%;margin:10px;" placeholder="name"><br>
    			<input type="number" class="form-control" id="fadcrd" style="width:50%;margin:10px;" placeholder="CRD NO"><br>
    			<input type="text" class="form-control" id="fadty" style="width:50%;margin:10px;" placeholder="TYPE"><br>
    			<input type="text" class="form-control" id="fadba" style="width:50%;margin:10px;" placeholder="BUSINESS ADDRESS"><br>
    			<input type="text" class="form-control" id="faded" style="width:50%;margin:10px;" placeholder="EFFECTIVE DATE : FORMAT-yyyy/mm/dd"><br>
    			<input type="text" class="form-control" id="fadde" style="width:50%;margin:10px;" placeholder="DESCRIPTION"><br>
    			<input type="text" class="form-control" id="fadfe" style="width:50%;margin:10px;" placeholder="FOREIGN ENTITY"><br>
    			<input type="text" class="form-control" id="fadco" style="width:50%;margin:10px;" placeholder="COUNTRY"><br>
    			<input type="text" class="form-control" id="fadsa" style="width:50%;margin:10px;" placeholder="SECURITY ACTIVITIES"><br>
    			<input type="text" class="form-control" id="fadid" style="width:50%;margin:10px;" placeholder="INVESTMENT ADVISORY"><br>
    			<button class="btn btn-info" style="position:relative;left:-100px;" type="button">EDIT</button>
<button class="btn btn-success" type="button" style="display:none;" onclick="firm_affliationdtls=1; editing_current=0;">SAVE</button>
<button class="btn btn-default" style="position: relative;left: 100px;" type="button">NEXT</button>
    		</div>
    	</form>
    </center>
    </div>
    <div id="menu5" class="tab-pane fade">
    	<center>
    		<p style="color: #5d92ba;padding:15px; font-size: 21px;">FIRM ARRANGEMENTS</p>
    		<form>
    			<div class="form-group">
    				<input type="number" class="form-control" id="farrcrd" style="width:50%;margin:10px;" placeholder="CRD NO"><br>
    				<label class="radio-inline">
    					<b style="position: relative;left:-176px;">Is CLEARING ARRANG OTHER BD?</b>
    				</label>
    				<label class="radio-inline" >

      <input type="radio" name="farrcaob" value="yes" >YES
    </label>
    <label class="radio-inline">
      <input type="radio" name="farrcaob" value="no" >NO
    </label><br>
    <br><label class="radio-inline">
    					<b style="position: relative;left:-190px;">Is INTRO ARRANG OTHER BD?</b>
    				</label>
    				<label class="radio-inline" style="position: relative; left:15px;">

      <input type="radio" name="farriiaob" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:15px;">
      <input type="radio" name="farriiaob" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
    					<b style="position: relative;left:-176px;">ARE BOOK RECORDS BY THIRD PARTY?</b>
    				</label>
    				<label class="radio-inline" style="position: relative; left:-17px;">

      <input type="radio" name="farrabrbtp" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-17px;">
      <input type="radio" name="farrabrbtp" value="no">NO
    </label>
<br>
    <br><label class="radio-inline">
    					<b style="position: relative;left:-176px;">ARE ACCOUNTS FUNDS SEC FROM THIRD PARTY?</b>
    				</label>
    				<label class="radio-inline" style="position: relative; left:-48px;">

      <input type="radio" name="farraafsftp" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-48px;">
      <input type="radio" name="farraafsftp" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
    					<b style="position: relative;left:-176px;">ARE CUSTOMER ACOOUNTS FUNDS SEC FROM THIRD PARTY?</b>
    				</label>
    				<label class="radio-inline" style="position: relative; left:-89px;">

      <input type="radio" name="farracafsftp" value="yes">YES

    </label>
    <label class="radio-inline" style="position: relative; left:-89px;">
      <input type="radio" name="farracafsftp" value="no">NO
    </label><br>
    <br><label class="radio-inline">
    					<b style="position: relative;left:-176px;">IS INDIVIDUAL CONTROL MANAGEMENT POLICIES?</b>
    				</label>
    				<label class="radio-inline" style="position: relative; left:-48px;">

      <input type="radio" name="farrincmp" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-48px;">
      <input type="radio" name="farrincmp" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
    					<b style="position: relative;left:-176px;">IS INDIVIDUAL FINANCE FIRM BUSINESS?</b>
    				</label>
    				<label class="radio-inline" style="position: relative; left:-13px;">

      <input type="radio" name="farriiffb" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-13px;">
      <input type="radio" name="farriiffb" value="no">NO
    </label><br><br><br>
    <button class="btn btn-info" style="position:relative;left:-100px;" type="button">EDIT</button>
<button class="btn btn-success" type="button" style="display:none;" onclick="firm_arrangement=1; editing_current=0;">SAVE</button>
<button class="btn btn-default" style="position: relative;left: 100px;" type="button">NEXT</button>
    			</div>
    		</form>
    	</center>
    </div>
     <div id="menu6" class="tab-pane fade">
      <center>
        <p style="color: #5d92ba;padding:15px; font-size: 21px;">FIRM BUSINESS TYPES</p>
      <form>
        <div class="form-group">
            <input type="number" class="form-control" id="fbtcrd" style="width:50%;margin:10px;" placeholder="CRD NO"><br>
            <label class="radio-inline">
              <b style="position: relative;left:-160px;">Is_ExcMbr_Comm_Bus?</b>
            </label>
            <label class="radio-inline" >

      <input type="radio" name="fbtiecm" value="yes">YES
    </label>
    <label class="radio-inline">
      <input type="radio" name="fbtiecm" value="no">NO
    </label><br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-190px;">Is_ExcMbr_Floor_Acti?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:6px;">

      <input type="radio" name="fbtiefa" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:6px;">
      <input type="radio" name="fbtiefa" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_BD_IDMkt_CorpSec_OTC?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:-13px;">

      <input type="radio" name="fbtibico" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-13px;">
      <input type="radio" name="fbtibico" value="no">NO
    </label>
<br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_BD_Retl_CorpEQ_OTC?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:-5px;">

      <input type="radio" name="fbtibrco" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-5px;">
      <input type="radio" name="fbtibrco" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_BD_Selling_CorpFI_Sec?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:-10px;">

      <input type="radio" name="fbtibscs" value="yes">YES

    </label>
    <label class="radio-inline" style="position: relative; left:-10px;">
      <input type="radio" name="fbtibscs" value="no">NO
    </label><br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_UW_SellGrpPrt_ExMF?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:00px;">

      <input type="radio" name="fbtisuse" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:00px;">
      <input type="radio" name="fbtisuse" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_UW_Sponsor_MF?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:16px;">

      <input type="radio" name="fbtiusm" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:16px;">
      <input type="radio" name="fbtiusm" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_MF_Retl?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:48px;">

      <input type="radio" name="fbtimr" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:48px;">
      <input type="radio" name="fbtimr" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_D_USGovSec?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:31px;">

      <input type="radio" name="fbtidu" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:31px;">
      <input type="radio" name="fbtidu" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_B_USGovSec?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:33px;">

      <input type="radio" name="fbtibu" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:33px;">
      <input type="radio" name="fbtibu" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_D_MuniSec?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:39px;">

      <input type="radio" name="fbtidm" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:39px;">
      <input type="radio" name="fbtidm" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_B_MuniSec?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:40px;">

      <input type="radio" name="fbtibm" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:40px;">
      <input type="radio" name="fbtibm" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_BD_Selling_VarLifeInsu_Annuities?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:-35px;">

      <input type="radio" name="fbtisbsva" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-35px;">
      <input type="radio" name="fbtisbsva" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_Solicitor_TD?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:39px;">

      <input type="radio" name="fbtist" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:39px;">
      <input type="radio" name="fbtist" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_RE_Syndicator?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:31px;">

      <input type="radio" name="fbtirs" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:31px;">
      <input type="radio" name="fbtirs" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_BD_Selling_OGInterests?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:00px;">

      <input type="radio" name="fbtibso" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:00px;">
      <input type="radio" name="fbtibso" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_BD_OptionWriter?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:25px;">

      <input type="radio" name="fbtibo" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:25px;">
      <input type="radio" name="fbtibo" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_BD_Selling_Sec1IssuerExMF?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:-14px;">

      <input type="radio" name="fbtibss" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-14px;">
      <input type="radio" name="fbtibss" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_BD_Selling_NPO?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:28px;">

      <input type="radio" name="fbtibsn" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:28px;">
      <input type="radio" name="fbtibsn" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_InvstAdvisoryServices?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:10px;">

      <input type="radio" name="fbtiias" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:10px;">
      <input type="radio" name="fbtiias" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_BD_Selling_TaxSh_LP_PrimaryMkt?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:-31px;">

      <input type="radio" name="fbtibstlp" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-31px;">
      <input type="radio" name="fbtibstlp" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_BD_Selling_TaxSh_LP_SecondaryMkt?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:-40px;">

      <input type="radio" name="fbtibstls" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-40px;">
      <input type="radio" name="fbtibstls" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_NExMbr_Arrng_ListedSec?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:2px;">

      <input type="radio" name="fbtinal" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:2px;">
      <input type="radio" name="fbtinal" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_Prop_TradingSec_Own?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:12px;">

      <input type="radio" name="fbtipt" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:12px;">
      <input type="radio" name="fbtipt" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_PrivatePlacement_Sec?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:13px;">

      <input type="radio" name="fbtipps" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:13px;">
      <input type="radio" name="fbtipps" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_BD_Selling_Mortgages?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:13px;">

      <input type="radio" name="fbtibsm" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:13px;">
      <input type="radio" name="fbtibsm" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_BD_networking_Banks?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:14px;">

      <input type="radio" name="fbtibnb" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:14px;">
      <input type="radio" name="fbtibnb" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_BD_networking_Insurance?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:3px;">

      <input type="radio" name="fbtibni" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:3px;">
      <input type="radio" name="fbtibni" value="no">NO
    </label>
    <br>
    <br><label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_OtherBusiness?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:42px;">

      <input type="radio" name="fbtiob" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:42px;">
      <input type="radio" name="fbtiob" value="no">NO
    </label>
    <br>
    <br>
    <button class="btn btn-info" style="position:relative;left:-100px;" type="button">EDIT</button>
<button class="btn btn-success" type="button" style="display:none;" onclick="firm_business_types=1; editing_current=0;">SAVE</button>
<button class="btn btn-default" style="position: relative;left: 100px;" type="button">NEXT</button>

          </div>
      </form>
     </div>
     <div id="menu7" class="tab-pane fade">
      <center>
        <p style="color: #5d92ba;padding:15px; font-size: 21px;">FIRM DO EO</p>
        <form>
          <div class="form-group">
            <input type="number" class="form-control" id="fdecrdno" style="width:50%;margin:10px;" placeholder="CRD NO"><br>
            <input type="number" class="form-control" id="fdeden" style="width:50%;margin:10px;" placeholder="DO_EO_NO"><br>
              <input type="text" class="form-control" id="fdedeln" style="width:50%;margin:10px;" placeholder="DO EO LEGAL NAME"><br>
              <input type="number" class="form-control" id="fdedecn" style="width:50%;margin:10px;" placeholder="DO_EO_CRD_NO"><br>
              <input type="text" class="form-control" id="fdedet" style="width:50%;margin:10px;" placeholder="DO_EO_TYPE"><br>
              <input type="text" class="form-control" id="fdedep" style="width:50%;margin:10px;" placeholder="position"><br>
              <input type="text" class="form-control" id="fdepsd" style="width:50%;margin:10px;" placeholder="position start date: format yyyy/mm/dd"><br>
              <input type="text" class="form-control" id="fdepo" style="width:50%;margin:10px;" placeholder="PERC_OWNERSHIP"><br>
              <label class="radio-inline">
              <b style="position: relative;left:-176px;">Does direct management policies?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:-12px;">

      <input type="radio" name="fdeddmp" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-12px;">
      <input type="radio" name="fdeddmp" value="no">NO
    </label><br>
    <br>
    <label class="radio-inline">
              <b style="position: relative;left:-176px;">is public reporting company?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:10px;">

      <input type="radio" name="fdeiprc" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:10px;">
      <input type="radio" name="fdeiprc" value="no">NO
    </label>
    <br>
    <br>
    <button class="btn btn-info" style="position:relative;left:-100px;" type="button">EDIT</button>
<button class="btn btn-success" type="button" style="display:none;" onclick="firm_do_eo=1; editing_current=0;">SAVE</button>
<button class="btn btn-default" style="position: relative;left: 100px;" type="button">NEXT</button>

</div>
</form>
</center>
</div>

          
          <div id="menu8" class="tab-pane fade">
      <center>
        <p style="color: #5d92ba;padding:15px; font-size: 21px;">FIRM INDIRECT OWNERS</p>
        <form>
          <div class="form-group">
            <input type="number" class="form-control" id="fiocrd" style="width:50%;margin:10px;" placeholder="CRD NO"><br>
            <input type="number" class="form-control" id="fioido" style="width:50%;margin:10px;" placeholder="IDO NO"><br>
            <input type="text" class="form-control" id="fioiln" style="width:50%;margin:10px;" placeholder="IDO LEGAL NAME"><br>
            <input type="number" class="form-control" id="fioicn" style="width:50%;margin:10px;" placeholder="IDO CRD NO"><br>
            <input type="text" class="form-control" id="fioit" style="width:50%;margin:10px;" placeholder="IDO TYPE"><br>
            <input type="text" class="form-control" id="fioiv" style="width:50%;margin:10px;" placeholder="IDO VIA"><br>
            <input type="text" class="form-control" id="fiortd" style="width:50%;margin:10px;" placeholder="REL TO DO"><br>
            <input type="text" class="form-control" id="fiored" style="width:50%;margin:10px;" placeholder="REL EST DATE: format yyyy/mm/dd"><br>
            <input type="text" class="form-control" id="fiopo" style="width:50%;margin:10px;" placeholder="PERC OWNERSHIP"><br>
            <label class="radio-inline">
              <b style="position: relative;left:-176px;">Does direct management policies?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:-10px;">

      <input type="radio" name="fioddmp" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-10px;">
      <input type="radio" name="fioddmp" value="no">NO
    </label><br>
    <br>
    <label class="radio-inline">
              <b style="position: relative;left:-176px;">is public reporting company?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:10px;">

      <input type="radio" name="fioiprc" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:10px;">
      <input type="radio" name="fioiprc" value="no">NO
    </label><br>
	<br>
	<button class="btn btn-info" style="position:relative;left:-100px;" type="button">EDIT</button>
<button class="btn btn-success" type="button" style="display:none;" onclick="firm_indirectowners=1; editing_current=0;">SAVE</button>
<button class="btn btn-default" style="position: relative;left: 100px;" type="button">NEXT</button>
          </div>

        </form>
      </center>
    </div>
    <div id="menu9" class="tab-pane fade">
      <center>
        <p style="color: #5d92ba;padding:15px; font-size: 21px;">FIRM OPERATIONS SUMMARY</p>
        <form>
          <div class="form-group">
             <input type="number" class="form-control" id="foscrd" style="width:50%;margin:10px;" placeholder="CRD NO"><br>
          
    <label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_Reg_with_sec?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:28px;">

      <input type="radio" name="fosirws" value="yes" >YES
    </label>
    <label class="radio-inline" style="position: relative; left:28px;">
      <input type="radio" name="fosirws" value="no">NO
    </label>
    <br>
    <br>
    <label class="radio-inline">
              <b style="position: relative;left:-176px;">is_reg_with_sro?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:32px;">

      <input type="radio" name="fosirwso" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:32px;">
      <input type="radio" name="fosirwso" value="no">NO
    </label>
    <br>
    <br>
    <label class="radio-inline">
              <b style="position: relative;left:-176px;">is_reg_with_statesTerrirtories?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:-15px;">

      <input type="radio" name="fosirwst" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-15px;">
      <input type="radio" name="fosirwst" value="no">NO
    </label>
     <input type="number" class="form-control" id="fosnsr" style="width:50%;margin:10px;" placeholder="No_sro_reg"><br>
      <input type="number" class="form-control" id="fosnstr" style="width:50%;margin:10px;" placeholder="No_states_territories_reg"><br>
      <label class="radio-inline">
              <b style="position: relative;left:-176px;">is_currently_suspended?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:-13px;">

      <input type="radio" name="fosics" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-13px;">
      <input type="radio" name="fosics" value="no">NO
    </label>
     <input type="number" class="form-control" id="fosntb" style="width:50%;margin:10px;" placeholder="No_type_businesses"><br>
     <label class="radio-inline">
              <b style="position: relative;left:-176px;">Is_Affi_Fin_Invst_Institutions?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:-13px;">

      <input type="radio" name="fosiafii" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-13px;">
      <input type="radio" name="fosiafii" value="no">NO
    </label>
    <br>
    <br>
    <label class="radio-inline">
              <b style="position: relative;left:-176px;">Has_Ref_Arr_Other_BD?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:-13px;">

      <input type="radio" name="foshraob" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-13px;">
      <input type="radio" name="foshraob" value="no">NO
    </label>
    <br>
    <br>
    <button class="btn btn-info" style="position:relative;left:-100px;" type="button">EDIT</button>
<button class="btn btn-success" type="button" style="display:none;" onclick="firm_operations_summary=1; editing_current=0;">SAVE</button>
<button class="btn btn-default" style="position: relative;left: 100px;" type="button">NEXT</button>
          </div>
        </form>
      </center>
    </div>
    <div id="menu10" class="tab-pane fade">
      <center>
        <p style="color: #5d92ba;padding:15px; font-size: 21px;">FIRM PROFILE</p>
        <form>
          <div class="form-group">
            <input type="text" class="form-control" id="fpfn" style="width:50%;margin:10px;" placeholder="firm name"><br>
            <input type="text" class="form-control" id="fpffln" style="width:50%;margin:10px;" placeholder="firm full legal name"><br>
            <input type="text" class="form-control" id="fpfdba" style="width:50%;margin:10px;" placeholder="firm doing business as"><br>
            <input type="number" class="form-control" id="fpcn" style="width:50%;margin:10px;" placeholder="CRD NO"><br>
            <input type="number" class="form-control" id="fpsn" style="width:50%;margin:10px;" placeholder="SEC NO"><br>
            <input type="text" class="form-control" id="fpfc" style="width:50%;margin:10px;" placeholder="firm classification"><br>
            <input type="text" class="form-control" id="fpffd" style="width:50%;margin:10px;" placeholder="firm founded date: format yyyy/mm/dd"><br>
            <input type="text" class="form-control" id="fpffl" style="width:50%;margin:10px;" placeholder="firm founded location"><br>
            <input type="text" class="form-control" id="fpffe" style="width:50%;margin:10px;" placeholder="Firm_FiscalYear_EndMonth"><br>
            <input type="text" class="form-control" id="fpbtn" style="width:50%;margin:10px;" placeholder="business tel no"><br>
            <input type="text" class="form-control" id="fprb" style="width:50%;margin:10px;" placeholder="regulated by"><br>
			<button class="btn btn-info" style="position:relative;left:-100px;" type="button">EDIT</button>
<button class="btn btn-success" type="button" style="display:none;" onclick="firm_profile=1; editing_current=0;">SAVE</button>
<button class="btn btn-default" style="position: relative;left: 100px;" type="button">NEXT</button>

          </div>
        </form>
      </center>
    </div>
    <div id="menu11" class="tab-pane fade">
      <center>
        <p style="color: #5d92ba;padding:15px; font-size: 21px;">FIRM SEC REG</p>
        <form>
          <div class="form-group">
            <input type="number" class="form-control" id="fsrcn" style="width:50%;margin:10px;" placeholder="CRD NO"><br>
            <label class="radio-inline">
              <b style="position: relative;left:-176px;">is bd?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:-13px;">

      <input type="radio" name="fsrib" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-13px;">
      <input type="radio" name="fsrib" value="no">NO
    </label><br><br>
    <label class="radio-inline">
              <b style="position: relative;left:-176px;">is cd and gsbd?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:-13px;">

      <input type="radio" name="fsricag" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-13px;">
      <input type="radio" name="fsricag" value="no">NO
    </label>
    <br><br>
    <label class="radio-inline">
              <b style="position: relative;left:-176px;">is gsbd only?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:-13px;">

      <input type="radio" name="fsigo" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-13px;">
      <input type="radio" name="fsigo" value="no">NO
    </label><br><br>
    <label class="radio-inline">
              <b style="position: relative;left:-176px;">is gsbd ceased?</b>
            </label>
            <label class="radio-inline" style="position: relative; left:-13px;">

      <input type="radio" name="fsigc" value="yes">YES
    </label>
    <label class="radio-inline" style="position: relative; left:-13px;">
      <input type="radio" name="fsigc" value="no">NO
    </label>
    <br>
    <br>
    <button class="btn btn-info" style="position:relative;left:-100px;" type="button">EDIT</button>
<button class="btn btn-success" type="button" style="display:none;" onclick="firm_sec_reg=1; editing_current=0;">SAVE</button>
<button class="btn btn-default" style="position: relative;left: 100px;" type="button">NEXT</button>
<button class="btn btn-default" style="position: relative;left: 100px;" type="button" id="finalsubmit">SUBMIT DETAILS</button>

          </div>
        </form>
      </center>
    </div>

</div>
</div>
</body>
</html>